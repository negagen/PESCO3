﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraEstanqueControllerPez : MonoBehaviour {
    public LotePecesController registroPeces;
    public GameObject target;
    public float turnSpeed = 1f;
    public Vector3 offset;

    private void Start()
    {
        //transform.Rotate(0, -90.0f, 0);
        target = registroPeces.peces[0];
    }
    private void Update()
    {
        
        //offset = transform.position - target.transform.position;
    }

    void FixedUpdate()
    {
        //transform.position = new Vector3(target.transform.position.x+1.5f, target.transform.position.y, target.transform.position.z);
        
        //transform.rotation = new Quaternion(0.0f, 0.0f, 0.0f, Time.deltaTime * turnSpeed);

        transform.position = Vector3.Lerp(transform.position, target.transform.position, Time.deltaTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, target.transform.rotation, Time.deltaTime * turnSpeed);
    }

}

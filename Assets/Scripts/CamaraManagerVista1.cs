﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CamaraManagerVista1 : MonoBehaviour {

    public Camera MainCamera;
    public Camera FishCamera;
    public Camera MercadoCamera;
    public Canvas CanvasEstanque;
    public Canvas CanvasMercado;

    private void Start()
    {
        ViewWorld();
    }
    // Atajos para ver las cámaras
    void Update () {
        if (Input.GetKey(KeyCode.Q))
        {
            ViewWorld();
        }
        else if (Input.GetKey(KeyCode.W))
        {
            ViewEstanque();
        }
        else if (Input.GetKey(KeyCode.E))
        {
            ViewMercado();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if ((this.gameObject.CompareTag("Player")) && (other.gameObject.tag == "GoalEstanque"))
        {
            ViewEstanque();
        }
        else
        if ((this.gameObject.CompareTag("Player")) && (other.gameObject.tag == "GoalMercado"))
        {
            ViewMercado();
        }
    }

    void OnTriggerExit()
    {
        ViewWorld();
    }

    public void ViewWorld()
    {
        //Ver la Camara Main
        MainCamera.gameObject.SetActive(true);
        FishCamera.gameObject.SetActive(false);
        MercadoCamera.gameObject.SetActive(false);
        //Ocultar los Canvas de los escenarios
        CanvasEstanque.gameObject.SetActive(false);
        CanvasMercado.gameObject.SetActive(false);
    }

    public void ViewEstanque()
    {
        //Ver la Camara del Estanque
        MainCamera.gameObject.SetActive(true);
        FishCamera.gameObject.SetActive(true);
        MercadoCamera.gameObject.SetActive(false);
        //Ver solo el Canvas del estanque
        CanvasEstanque.gameObject.SetActive(true);
        CanvasMercado.gameObject.SetActive(false);
    }

    public void ViewMercado()
    {
        //Ver la Camara del Mercado
        MainCamera.gameObject.SetActive(true);
        FishCamera.gameObject.SetActive(false);
        MercadoCamera.gameObject.SetActive(true);
        //Ver solo el Canvas del mercado
        CanvasEstanque.gameObject.SetActive(false);
        CanvasMercado.gameObject.SetActive(true);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CamaraManagerVista2 : MonoBehaviour {

    public Camera MainCamera;
    public Camera FishCamera;
    public Camera InsumosCamera;
    public Camera MercadoCamera;
    public Canvas CanvasInsumos;
    public Canvas CanvasEstanque;
    public Canvas CanvasMercado;

    private void Start()
    {
        ViewWorld();
    }

    // Atajos para ver las cámaras
    void Update () {
        if (Input.GetKey(KeyCode.Q))
        {
            ViewWorld();
        }
        else if (Input.GetKey(KeyCode.W))
        {
            ViewEstanque();
        }
        else if (Input.GetKey(KeyCode.E))
        {
            ViewInsumos();
        }
        else if (Input.GetKey(KeyCode.R))
        {
            ViewMercado();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if ((this.gameObject.CompareTag("Player")) && (other.gameObject.tag == "GoalInsumos"))
        {
            ViewInsumos();
        }
        else
        if ((this.gameObject.CompareTag("Player")) && (other.gameObject.tag == "GoalEstanque"))
        {
            ViewEstanque();
        }
        else
        if ((this.gameObject.CompareTag("Player")) && (other.gameObject.tag == "GoalMercado"))
        {
            ViewMercado();
        }
    }

    void OnTriggerExit()
    {
        ViewWorld();
    }

    public void ViewWorld()
    {
        //Ver la Camara Main
        MainCamera.gameObject.SetActive(true);
        InsumosCamera.gameObject.SetActive(false);
        FishCamera.gameObject.SetActive(false);
        MercadoCamera.gameObject.SetActive(false);
        //Ocultar los Canvas de los escenarios
        CanvasInsumos.gameObject.SetActive(false);
        CanvasEstanque.gameObject.SetActive(false);
        CanvasMercado.gameObject.SetActive(false);
    }

    public void ViewInsumos()
    {
        //Ver la Camara del Estanque
        MainCamera.gameObject.SetActive(true);
        InsumosCamera.gameObject.SetActive(true);
        FishCamera.gameObject.SetActive(false);
        MercadoCamera.gameObject.SetActive(false);
        //Ver solo el Canvas de insumos
        CanvasInsumos.gameObject.SetActive(true);
        CanvasEstanque.gameObject.SetActive(false);
        CanvasMercado.gameObject.SetActive(false);
    }

    public void ViewEstanque()
    {
        //Ver la Camara del Estanque
        MainCamera.gameObject.SetActive(true);
        InsumosCamera.gameObject.SetActive(false);
        FishCamera.gameObject.SetActive(true);
        MercadoCamera.gameObject.SetActive(false);
        //Ver solo el Canvas del estanque
        CanvasInsumos.gameObject.SetActive(false);
        CanvasEstanque.gameObject.SetActive(true);
        CanvasMercado.gameObject.SetActive(false);
    }

    public void ViewMercado()
    {
        //Ver la Camara del Mercado
        MainCamera.gameObject.SetActive(true);
        InsumosCamera.gameObject.SetActive(false);
        FishCamera.gameObject.SetActive(false);
        MercadoCamera.gameObject.SetActive(true);
        //Ver solo el Canvas del mercado
        CanvasInsumos.gameObject.SetActive(false);
        CanvasEstanque.gameObject.SetActive(false);
        CanvasMercado.gameObject.SetActive(true);
    }

}

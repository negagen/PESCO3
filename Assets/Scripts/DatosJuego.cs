﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DatosJuego 
{

    public string nombreJugador;
    public string sesionJugador;
    public double dineroDisponible;
    public int inventarioPeces;
    public double inventarioAlimento;
    public int tiempoIteracion;
    public bool estadoProduccion;
    public string modalidadVenta;
    public int turnoVenta;
    public bool unidoGremio;

    public double pesoPez;
    public double totalCostos;
    public double consumoReal;
    public double coberturaPesoMaximo;
    public double factorConversion;
    public double incrementoPesoPez;
    public double totalAlimentoSuministrado;
    public double incrementoGastos;
    public double temperatura;
    public double racionAlimenticiaRequerida;
    public double racionAlimenticiaASuministrar;
    public double racionAlimenticiaSuministrada;
    public double oxigenoDisuelto;
    public int pecesMuertos;
    public double costoInicialPeces;
    public int inventarioInicialPeces;
    public double costoMantenimientoDiario;
    public double costoTotalAlimento;
    public double totalVentas;
    public int totalPecesVendidos;

    public void AsignarValores(string[] datos)
    {
        nombreJugador = datos[0];
        sesionJugador = datos[1];
        dineroDisponible = double.Parse(datos[2]);
        inventarioPeces = int.Parse(datos[3]);
        inventarioAlimento = double.Parse(datos[4]);
        tiempoIteracion = int.Parse(datos[5]);
        estadoProduccion = bool.Parse(datos[6]);
        modalidadVenta = datos[7];
        turnoVenta = int.Parse(datos[8]);
        unidoGremio = bool.Parse(datos[9]);

        pesoPez = double.Parse(datos[10]);
        totalCostos = double.Parse(datos[11]);
        consumoReal = double.Parse(datos[12]);
        coberturaPesoMaximo = double.Parse(datos[13]);
        factorConversion = double.Parse(datos[14]);
        incrementoPesoPez = double.Parse(datos[15]);
        totalAlimentoSuministrado = double.Parse(datos[16]);
        incrementoGastos = double.Parse(datos[17]);
        temperatura = double.Parse(datos[18]);
        racionAlimenticiaRequerida = double.Parse(datos[19]);
        racionAlimenticiaASuministrar = double.Parse(datos[20]);
        racionAlimenticiaSuministrada = double.Parse(datos[21]);
        oxigenoDisuelto = double.Parse(datos[22]);
        pecesMuertos = int.Parse(datos[23]);
        costoInicialPeces = double.Parse(datos[24]);
        inventarioInicialPeces = int.Parse(datos[25]);
        costoMantenimientoDiario = double.Parse(datos[26]);
        costoTotalAlimento = double.Parse(datos[27]);
        totalVentas = double.Parse(datos[28]);
        totalPecesVendidos = int.Parse(datos[29]);
    }

    public void CargarDatos(string[] datos)
    {
        datos[0] = nombreJugador;
        datos[1] = sesionJugador;
        datos[2] = dineroDisponible.ToString();
        datos[3] = inventarioPeces.ToString();
        datos[4] = inventarioAlimento.ToString();
        datos[5] = tiempoIteracion.ToString();
        datos[6] = estadoProduccion.ToString();
        datos[7] = modalidadVenta;
        datos[8] = turnoVenta.ToString();
        datos[9] = unidoGremio.ToString();

        datos[10] = pesoPez.ToString();
        datos[11] = totalCostos.ToString();
        datos[12] = consumoReal.ToString();
        datos[13] = coberturaPesoMaximo.ToString();
        datos[14] = factorConversion.ToString();
        datos[15] = incrementoPesoPez.ToString();
        datos[16] = totalAlimentoSuministrado.ToString();
        datos[17] = incrementoGastos.ToString();
        datos[18] = temperatura.ToString();
        datos[19] = racionAlimenticiaRequerida.ToString();
        datos[20] = racionAlimenticiaASuministrar.ToString();
        datos[21] = racionAlimenticiaSuministrada.ToString();
        datos[22] = oxigenoDisuelto.ToString();
        datos[23] = pecesMuertos.ToString();
        datos[24] = costoInicialPeces.ToString();
        datos[25] = inventarioInicialPeces.ToString();
        datos[26] = costoMantenimientoDiario.ToString();
        datos[27] = costoTotalAlimento.ToString();
        datos[28] = totalVentas.ToString();
        datos[29] = totalPecesVendidos.ToString();
    }
}

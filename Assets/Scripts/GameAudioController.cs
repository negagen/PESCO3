﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class GameAudioController : MonoBehaviour {

    public AudioMixer musicaPesco;
    public Slider volumenMusica;
    public Slider volumenEfectos;
    private bool muteFX;
    private bool muteMusic;

    //Controladores de audio
    public AudioMixerSnapshot bajoagua;
    public AudioMixerSnapshot corriendo;
    public AudioMixerSnapshot pausado;
    public Camera camaraEstanque;

    // Use this for initialization
    void Start()
    {
        muteMusic = false;
        muteFX = false;
    }

    private void Update()
    {
        ControlSonido();
    }
    public void setVolMusica()
    {
        if (muteMusic == true)
        {
            musicaPesco.SetFloat("VolMusica", volumenMusica.value);
            muteMusic = false;
        }else
        {
            musicaPesco.SetFloat("VolMusica", -80.0f);
            muteMusic = true;
        }
    }

    public void setVolFX()
    {
        if (muteFX == true)
        {
            musicaPesco.SetFloat("VolFX", volumenEfectos.value);
            muteFX = false;
        }
        else
        {
            musicaPesco.SetFloat("VolFX", -80.0f);
            muteFX = true;
        }
    }

    //Configuración de volumen de música con slider
    public void CambiarVolumenMusica(float value)
    {
        musicaPesco.SetFloat("VolMusica", volumenMusica.value);
    }

    //Configuración de volumen de música con slider
    public void CambiarVolumenEfectos(float value)
    {
        musicaPesco.SetFloat("VolFX", volumenEfectos.value);
    }

    //Función para validar el sonido de fondo de acuerdo a los estados y vistas del juego
    private void ControlSonido()
    {
        if (camaraEstanque.isActiveAndEnabled)
        {
            if (Time.timeScale==0)
            {
                pausado.TransitionTo(0.01f);
            }
            else
            {
                bajoagua.TransitionTo(0.01f);
            }
        }
        else
        {
            if (Time.timeScale==0)
            {
                pausado.TransitionTo(0.01f);
            }
            else
            {
                corriendo.TransitionTo(0.01f);
            }
        }
    }
}

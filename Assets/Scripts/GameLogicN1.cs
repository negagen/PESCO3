﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Collections;


public class GameLogicN1 : MonoBehaviour {

    /*En este sccript se colocan todas las acciones lógicas que producen un cambio en el juego
    (Efectos de pulsar los botones)*/

    //Variables de control para los estados del juego
    public bool isPaused = false;
    public bool estadoProduccion = false;
    //Variables de control de producción
    private bool errorPeso = false;
    private bool errorConexion = false;
    private bool guardandoPartida = false;
    [SerializeField]
    [Tooltip("Control para saber si hay datos registrados en la BD")]
    public string[] registroPartida;
    [Tooltip("Formato para registro de datos de partida en la BD")]
    public string[] datosPartida;
    [Tooltip("Formato para registro de venta en la BD")]
    public string[] datosVenta;
    public DatosJuego gameData = new DatosJuego();
    public DatosVenta ventaData = new DatosVenta();

    //Variables de operación para el juego
    private int pecesAVender;
    private double totalVentaPeces;
    private double costoTotalProduccion;
    private double gananciaVenta;
    public GameObject produccion;
    public GameObject mojarraIntro;

    //Datos del jugador
    private string nombreJugador;
    private string sesionJugador;
    private double dineroDisponible;
    private int inventarioPeces;
    private bool unidoGremio = false;

    //Precios del nivel
    private int VALORUNITARIOPEZ;
    [SerializeField]
    private double precioVentaPez;
    private float PESOMINIMO;

    //Datos del servidor
    private int tiempoMercado;

    //Elementos UI necesarios para capturar los valores de operación y calcular totales
    //Venta de peces
    public Text PMValorTotalVenta;
    public Text PMDecisionVentaPeces;
    public Text PMGanancia;
    public Text LWGanancia;
    public Text GOGanancia;

    // Método invocado sin tener en cuenta si el script está activo
    private void Awake()
    {
        Time.timeScale = 1;
        VALORUNITARIOPEZ = 2;
        PESOMINIMO = 0.125f;
        precioVentaPez = 8000;
        dineroDisponible = 1000;

        costoTotalProduccion = 0;
        gananciaVenta = 0;
    }

    // Método invocado la primera vez que actúa el script
    void Start () {
        isPaused = false;

        nombreJugador = GestionBD.singleton.GetNombreJugador();
        sesionJugador = GestionBD.singleton.GetNombreSesion();
        tiempoMercado = GestionBD.singleton.GetTiempoMercado();

        datosPartida = new string[30];
        registroPartida = new string[30];
        datosVenta = new string[7];
        //produccion.GetComponent<PlayTimer>().FijarDuracionDia(GestionBD.singleton.GetTiempoIteracion());
        produccion.SetActive(false);
        //Traer datos de la BD de datos en caso de que existan
        CargarPartida();
        //ConsultarPrecio();
    }

    // Update is called once per frame
    void Update()
    {
        //La lógica del juego debe estar mirando si hay una producción activa.
        estadoProduccion = produccion.GetComponent<ModeloN1>().GetProduccionActiva();
        CalcularValorVentaPeces();
        CalcularEscalaAlimento(produccion.GetComponent<ModeloN1>().GetRacionAlimenticia());
        if (Input.GetKeyDown("p"))
        {
            Pause();
        }
    }

    //Función para Pausar
    public void Pause()
    {
        if (isPaused == true)//¿El juego está pausado?
        {//Hagalo correr
            Time.timeScale = 1;
            isPaused = false;
        }
        else if(isPaused==false)
        {//Hagalo pausar
            Time.timeScale = 0;
            isPaused = true;
        }
    }

    public void VenderPeces()
    {
        double subtotal = dineroDisponible + totalVentaPeces;
        int nuevoInventarioPeces = inventarioPeces - pecesAVender;
        //Modificación de inventarios
        SetInventarioPeces(nuevoInventarioPeces);
        SetDineroDisponible(subtotal);
        //Actualización de datos en el modelo
        FinalizarProduccion(nuevoInventarioPeces);
        //Guardo los datos de venta y luego acabo el modelo y los peces en el estanque
        gananciaVenta = totalVentaPeces - costoTotalProduccion;
        PMGanancia.text = "$ " + gananciaVenta.ToString("N0");
        LWGanancia.text = "$ " + gananciaVenta.ToString("N0");
        GOGanancia.text = "$ " + gananciaVenta.ToString("N0");
        estadoProduccion = false;
        Debug.Log("Venta realizada a los " + Time.time + ".");
        SalvarVenta();
        SalvarEstado();
    }

    //Función que calcula el valor a pagar por un determinado número de peces, toma el contenido del input y multiplica por el precio del pez
    public void CalcularValorVentaPeces()
    {
        if (estadoProduccion == true)
        {
            pecesAVender = inventarioPeces;
            costoTotalProduccion = Math.Round(produccion.GetComponent<ModeloN1>().GetCostoProduccion(),2);
            totalVentaPeces = precioVentaPez * PesoPez();
            PMValorTotalVenta.text = "Valor a recibir:\n" + "$ " + totalVentaPeces.ToString("N0");
            PMDecisionVentaPeces.text = "¿Quieres vender tu pez?";
        }
        else
        {
            PMValorTotalVenta.text = "Total de la venta";
            PMDecisionVentaPeces.text = "";
        }

    }

    //Método para convertir el peso promedio del pez en kg
    private double PesoPez()
    {
        double peso = produccion.GetComponent<ModeloN1>().GetPesoPromedio();
        return Math.Round((peso / 1000), 3);
    }

    private void IniciarProduccion(int peces)
    {
        //Activación de una nueva producción
        produccion.SetActive(true);
        produccion.GetComponent<PlayTimer>().FijarDuracionDia(GestionBD.singleton.GetTiempoIteracion());
        produccion.GetComponent<ModeloN1>().timer.CorrerTiempo();
        produccion.GetComponent<ModeloN1>().SetProduccionActiva(true);
        produccion.GetComponent<ModeloN1>().SetPecesenLoteP(peces);
        produccion.GetComponent<LotePecesController>().SetPezCount(peces);
        CalcularEscalaAlimento(produccion.GetComponent<ModeloN1>().GetRacionAlimenticia());
    }

    private void FinalizarProduccion(int peces)
    {
        //Activación de una nueva producción
        produccion.SetActive(false);
        produccion.GetComponent<ModeloN1>().SetProduccionActiva(false);
        produccion.GetComponent<ModeloN1>().SetPecesenLoteP(peces);
        produccion.GetComponent<LotePecesController>().SetPezCount(peces);
        produccion.GetComponent<ModeloN1>().SumarVenta(totalVentaPeces);
    }

    //Función para ajustar visualmente el tamaño de la ración por medio de la escala del GameObject
    private void CalcularEscalaAlimento(double racionActual)
    {
        try {
            if (racionActual == 0)
            {
                produccion.GetComponent<LotePecesController>().goal.GetComponent<MeshRenderer>().enabled = false;
            }
            else
            {
                produccion.GetComponent<LotePecesController>().goal.GetComponent<MeshRenderer>().enabled = true;
                if (racionActual <= 1)
                {
                    produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                }
                else
                {
                    if (racionActual <= 2)
                    {
                        produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                    }
                    else
                    {
                        if (racionActual <= 3)
                        {
                            produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                        }
                        else
                        {
                            if (racionActual <= 4)
                            {
                                produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                            }
                            else
                            {
                                produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                            }
                        }
                    }
                }
            }
        }
        catch (UnassignedReferenceException e)
        {
            // This happens when GetComponent throws UnassignedReference exception
            Debug.Log(e);
        }
    }

    // --- METODOS GETER AND SETER ----

    // --- Metodos Getter ---

    public double GetInventarioPeces()
    {
        return this.inventarioPeces;
    }

    public double GetDineroDisponible()
    {
        return this.dineroDisponible;
    }

    public double GetValorUnitarioPez()
    {
        return this.VALORUNITARIOPEZ;
    }

    public double GetPrecioVentaPez()
    {
        return this.precioVentaPez;
    }

    public bool GetEstadoProduccion()
    {
        return this.estadoProduccion;
    }

    public bool GetErrorPeso()
    {
        return this.errorPeso;
    }

    public bool GetErrorConexion()
    {
        return this.errorConexion;
    }

    // --- Metodos Setter ---
    private void SetInventarioPeces(int peces)
    {
        this.inventarioPeces = peces;
    }

    public void SetDineroDisponible(double saldo)
    {
        this.dineroDisponible = saldo;
    }

    public bool GetGuardandoPartida()
    {
        return this.guardandoPartida;
    }

    public void SetErrorPeso(bool estado)
    {
        this.errorPeso = estado;
    }

    public void SetErrorConexion(bool estado)
    {
        this.errorConexion = estado;
    }

    // --- METODOS PARA MANEJO DE ESCENAS ----

    public void ReiniciarNivel()
    {
        //Eliminar datos
        GestionBD.singleton.EliminarEstadoJuego();
        //Recargue la escena
        Scene nivelActual = SceneManager.GetActiveScene();
        Debug.Log("Reiniciando el " + nivelActual.name);
        Time.timeScale = 1;
        SceneManager.LoadScene(nivelActual.name,LoadSceneMode.Single);
    }

    public void VolverHome()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MenuPrincipal");
    }

    // --- METODOS PARA MANEJO DE DATOS ----

    //Función para cargar los datos del juego para continuar jugando
    private void CargarPartida()
    {
        StartCoroutine(CargarEstadoPartida());
    }

    private IEnumerator CargarEstadoPartida()
    {
        yield return new WaitForSeconds(1);
        //Aquí debe actualizar los datos del jugador, modelo y peces en pantalla junto a su tamaño de acuerdo al peso
        if (GestionBD.singleton.GetRegistroData())
        {
            GestionBD.singleton.registroGameData.CargarDatos(registroPartida);
            //Consultar de BD el último registro
            produccion.SetActive(true);
            yield return new WaitForSeconds(1);
            this.nombreJugador = registroPartida[0];
            this.sesionJugador = registroPartida[1];
            SetDineroDisponible(double.Parse(registroPartida[2]));
            SetInventarioPeces(int.Parse(registroPartida[3]));
            produccion.GetComponent<ModeloN1>().SetPecesenLoteP(int.Parse(registroPartida[3]));
            produccion.GetComponent<LotePecesController>().SetPezCount(int.Parse(registroPartida[3]));
            produccion.GetComponent<ModeloN1>().SetTiempoIteracion(int.Parse(registroPartida[5]));
            produccion.GetComponent<PlayTimer>().SetTotalDias(int.Parse(registroPartida[5]));
            this.estadoProduccion = bool.Parse(registroPartida[6]);

            string[] datosParaModelo = new string[produccion.GetComponent<ModeloN1>().datosModelo.Length];
            Array.Copy(registroPartida, 10, datosParaModelo, 0, datosParaModelo.Length);
            produccion.GetComponent<ModeloN1>().SetDatosModelo(datosParaModelo);

            produccion.GetComponent<PlayTimer>().FijarDuracionDia(GestionBD.singleton.GetTiempoIteracion());
            produccion.GetComponent<ModeloN1>().timer.CorrerTiempo();

            print("Actualizados los datos.--------------");
            mojarraIntro.SetActive(false);
            GestionBD.singleton.SetRegistroData(false);
        }
        else
        {
            yield return new WaitForSeconds(4);
            IniciarProduccion(1);
            this.inventarioPeces = 1;
            mojarraIntro.SetActive(false);
        }
    }

    //Función para guardar el estado del juego
    public void SalvarEstado()
    {
        StartCoroutine(GuardarPartida());
    }

    private IEnumerator GuardarPartida()
    {
        guardandoPartida = true;
        GenerarDatosPartida();
        gameData.AsignarValores(this.datosPartida);
        string json = JsonUtility.ToJson(gameData);
        print(json);
        GestionBD.singleton.GuardarEstadoJuego(json);
        print("Guardado de datos realizado con éxito" + Time.time);
        yield return new WaitForSeconds(1);
        guardandoPartida = false;
    }

    //Función para guardar los datos de la partida
    private void GenerarDatosPartida()
    {
        /*Estructura de partida
         * [0] User
         * [1] Sesion
         * [2] Dinero
         * [3] Inventario peces
         * [4] Inventario alimento
         * [5] Tiempo iteracion
         * [6] Estado produccion
         * [7] Modalidad de venta
         * [8] Turno de venta
         * [9] Unión a gremio
         * [10-29] Datos del modelo
         */
        datosPartida[0] = nombreJugador;
        datosPartida[1] = sesionJugador;
        datosPartida[2] = this.dineroDisponible.ToString();
        datosPartida[3] = this.inventarioPeces.ToString(); ;
        datosPartida[4] = 0.ToString();
        datosPartida[5] = (produccion.GetComponent<ModeloN1>().GetTiempoIteracion()).ToString();
        datosPartida[6] = this.estadoProduccion.ToString();
        datosPartida[7] = "libre";
        datosPartida[8] = 0.ToString(); ;
        datosPartida[9] = unidoGremio.ToString(); ;
        if (this.estadoProduccion)
        {
            for (int i = 0; i < produccion.GetComponent<ModeloN1>().datosModelo.Length; i++)
            {
                datosPartida[10 + i] = produccion.GetComponent<ModeloN1>().datosModelo[i];
            }
        }
        else
        {
            for (int j = 10; j < 30; j++)
            {
                datosPartida[j] = 0.ToString();
            }
        }
    }

    public void SalvarVenta()
    {
        StartCoroutine(GuardarVenta());
    }

    private IEnumerator GuardarVenta()
    {
        guardandoPartida = true;
        GenerarDatosVenta();
        ventaData.AsignarValores(this.datosVenta);
        string jsonventa = JsonUtility.ToJson(ventaData);
        print(jsonventa);
        GestionBD.singleton.GuardarVenta(jsonventa);
        if (GestionBD.singleton.GetEstadoVenta())
        {
            print("Guardado de venta realizado con éxito" + Time.time);
        }
        yield return new WaitForSeconds(1);
        guardandoPartida = false;
    }

    //Función para guardar los datos de venta
    private void GenerarDatosVenta()
    {
        /*Estructura de venta
         * [0] User
         * [1] Sesion
         * [2] Peso
         * [3] Número de peces 
         * [4] Valor de la venta
         * [5] Modalidad de venta
         * [6] Costo acumulado
         */
        datosVenta[0] = nombreJugador;
        datosVenta[1] = sesionJugador;
        datosVenta[2] = produccion.GetComponent<ModeloN1>().GetPesoPromedio().ToString();
        datosVenta[3] = pecesAVender.ToString();
        datosVenta[4] = totalVentaPeces.ToString();
        datosVenta[5] = "libre";
        datosVenta[6] = produccion.GetComponent<ModeloN1>().GetCostoProduccion().ToString();
    }

    //Función para consultar el precio del mercado
    public void ConsultarPrecio()
    {
        StartCoroutine(PrecioMercado());
        print("Consultando precio del mercado...");
    }

    private IEnumerator PrecioMercado()
    {
        while (true)
        {
            GestionBD.singleton.ConsultarDatosMercado();
            yield return new WaitForSeconds(1f);
            this.precioVentaPez = GestionBD.singleton.GetPrecioMercado();
            yield return new WaitForSeconds(tiempoMercado);
        }
    }

}

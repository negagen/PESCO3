﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Collections;

public class GameLogicN2 : MonoBehaviour {

    /*En este sccript se colocan todas las acciones lógicas que producen un cambio en el juego
    (Efectos de pulsar los botones)*/

    //Variables de control para los estados del juego
    public bool isPaused = false;
    public bool estadoProduccion = false;
    private bool guardandoPartida = false;
    [SerializeField]
    [Tooltip("Control para saber si hay datos registrados en la BD")]
    public string[] registroPartida;
    [Tooltip("Formato para registro de datos de partida en la BD")]
    public string[] datosPartida;
    [Tooltip("Formato para registro de venta en la BD")]
    public string[] datosVenta;
    public DatosJuego gameData = new DatosJuego();
    public DatosVenta ventaData = new DatosVenta();

    //Variables de operación para el juego
    private float gramosAlimentoASuministrar;
    private int pecesAVender;
    private double totalVentaPeces;
    private double costoTotalProduccion;
    private double gananciaVenta;
    public GameObject produccion;
    public GameObject mojarraIntro;

    //Datos del jugador
    private string nombreJugador;
    private string sesionJugador;
    private double dineroDisponible;
    private int inventarioPeces;
    private bool unidoGremio = false;

    //Precios del nivel
    private int VALORUNITARIOPEZ;
    [SerializeField]
    private double precioVentaPez;
    private float PESOMINIMO;

    //Datos del servidor
    private int tiempoMercado;

    //Elementos UI necesarios para capturar los valores de operación y calcular totales
    //Suministro de ración
    public InputField entradaRacion;
    //Venta de peces
    public Text PMValorTotalVenta;
    public Text PMDecisionVentaPeces;
    public Text PMGanancia;
    public Text LWGanancia;
    public Text GOGanancia;

    //Variables de control sobre entradas nulas o no válidas
    private bool entradaRacionNula = true;
    private bool entradaVentaPecesNula = true;


    private void Awake()
    {
        Time.timeScale = 1;
        VALORUNITARIOPEZ = 2;
        precioVentaPez = 8000;
        dineroDisponible = 1000;
        PESOMINIMO = 0.125f;

        costoTotalProduccion = 0;
        gananciaVenta = 0;
    }

    // Use this for initialization
    void Start() {
        isPaused = false;
        CalcularValorSuministroRacion();
        CalcularValorVentaPeces();

        nombreJugador = GestionBD.singleton.GetNombreJugador();
        sesionJugador = GestionBD.singleton.GetNombreSesion();
        tiempoMercado = GestionBD.singleton.GetTiempoMercado();

        datosPartida = new string[30];
        registroPartida = new string[30];
        datosVenta = new string[7];
        //produccion.GetComponent<PlayTimer>().FijarDuracionDia(GestionBD.singleton.GetTiempoIteracion());
        produccion.SetActive(false);
        //Traer datos de la BD de datos en caso de que existan
        CargarPartida();
        ConsultarPrecio();
    }

    // Update is called once per frame
    void Update()
    {
        //La lógica del juego debe estar mirando si hay una producción activa.
        estadoProduccion = produccion.GetComponent<ModeloN2>().GetProduccionActiva();
        CalcularValorVentaPeces();
        if (Input.GetKeyDown("p"))
        {
            Pause();
        }
    }

    //Función para Pausar, tomado el bool si está pausado y hace las transiciones en el sonidos para producir los efectos
    public void Pause()
    {
        if (isPaused == true)//¿El juego está pausado?
        {//Hagalo correr
            Time.timeScale = 1;
            isPaused = false;
        }
        else if (isPaused == false)
        {//Hagalo pausar
            Time.timeScale = 0;
            isPaused = true;
        }
    }

    //Método para cambiar la ración de alimento que se suministra al estanque
    public void CambiarRacion()
    {
        double racionLote = gramosAlimentoASuministrar * inventarioPeces;
        CambiarRacionAlimenticia(gramosAlimentoASuministrar);
        Debug.Log("Modificación de la ración de alimento efectuada a los " + Time.time + ".");
        SalvarEstado();
        entradaRacion.text = null;
    }

    public void CalcularValorSuministroRacion()
    {
        if (entradaRacion.text != "" && !(entradaRacion.text.Contains("-")))
        {
            float.TryParse(entradaRacion.text, out gramosAlimentoASuministrar);
            entradaRacionNula = false;
        }
        else
        {
            entradaRacionNula = true;
        }
    }

    public void VenderPeces()
    {
        double subtotal = dineroDisponible + totalVentaPeces;
        int nuevoInventarioPeces = inventarioPeces - pecesAVender;
        //Modificación de inventarios
        SetInventarioPeces(nuevoInventarioPeces);
        SetDineroDisponible(subtotal);
        //Actualización de datos en el modelo
        FinalizarProduccion(nuevoInventarioPeces);
        //Guardo los datos de venta y luego acabo el modelo y los peces en el estanque
        gananciaVenta = totalVentaPeces - costoTotalProduccion;
        PMGanancia.text = "$ " + gananciaVenta.ToString("N0");
        LWGanancia.text = "$ " + gananciaVenta.ToString("N0");
        GOGanancia.text = "$ " + gananciaVenta.ToString("N0");
        estadoProduccion = false;
        Debug.Log("Venta realizada a los " + Time.time + ".");
        SalvarVenta();
        SalvarEstado();
    }

    //Función que calcula el valor a pagar por un determinado número de peces, toma el contenido del input y multiplica por el precio del pez
    public void CalcularValorVentaPeces()
    {
        if (estadoProduccion == true)
        {
            pecesAVender = inventarioPeces;
            costoTotalProduccion = Math.Round(produccion.GetComponent<ModeloN2>().GetCostoProduccion(), 2);
            totalVentaPeces = precioVentaPez * PesoPez();
            PMValorTotalVenta.text = "Valor a recibir:\n" + "$ " + totalVentaPeces.ToString("N0");
            PMDecisionVentaPeces.text = "¿Quieres vender tu pez?";
        }
        else
        {
            PMValorTotalVenta.text = "Total de la venta";
            PMDecisionVentaPeces.text = "";
        }

    }

    //Método para convertir el peso promedio del pez en kg
    private double PesoPez()
    {
        double peso = produccion.GetComponent<ModeloN2>().GetPesoPromedio();
        return Math.Round((peso / 1000), 3);
    }

    private void IniciarProduccion(int peces)
    {
        //Activación de una nueva producción
        produccion.SetActive(true);
        produccion.GetComponent<PlayTimer>().FijarDuracionDia(GestionBD.singleton.GetTiempoIteracion());
        produccion.GetComponent<ModeloN2>().timer.CorrerTiempo();
        produccion.GetComponent<ModeloN2>().SetProduccionActiva(true);
        produccion.GetComponent<ModeloN2>().SetPecesenLoteP(peces);
        produccion.GetComponent<LotePecesController>().SetPezCount(peces);
        CalcularEscalaAlimento(0);
    }

    private void FinalizarProduccion(int peces)
    {
        //Activación de una nueva producción
        produccion.GetComponent<ModeloN2>().SetProduccionActiva(false);
        produccion.GetComponent<ModeloN2>().SetPecesenLoteP(peces);
        produccion.GetComponent<LotePecesController>().SetPezCount(peces);
        produccion.GetComponent<ModeloN2>().SumarVenta(totalVentaPeces);
    }

    private void CambiarRacionAlimenticia(double nuevaracion)
    {
        produccion.GetComponent<ModeloN2>().SetRacionAlimenticiaP(nuevaracion);
        CalcularEscalaAlimento(nuevaracion);
    }

    //Función para ajustar visualmente el tamaño de la ración por medio de la escala del GameObject
    private void CalcularEscalaAlimento(double racionActual)
    {
        if (racionActual == 0)
        {
            produccion.GetComponent<LotePecesController>().goal.GetComponent<MeshRenderer>().enabled = false;
        }
        else
        {
            produccion.GetComponent<LotePecesController>().goal.GetComponent<MeshRenderer>().enabled = true;
            if (racionActual <= 1)
            {
                produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            }
            else
            {
                if (racionActual <= 2)
                {
                    produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                }
                else
                {
                    if (racionActual <= 3)
                    {
                        produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                    }
                    else
                    {
                        if (racionActual <= 4)
                        {
                            produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                        }
                        else
                        {
                            produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                        }
                    }
                }
            }
        }
    }

    // --- METODOS GETER AND SETER ----

    // --- Metodos Getter ---

    public double GetInventarioPeces()
    {
        return this.inventarioPeces;
    }

    public double GetDineroDisponible()
    {
        return this.dineroDisponible;
    }

    public double GetPrecioVentaPez()
    {
        return this.precioVentaPez;
    }

    public double GetTotalVenta()
    {
        return this.totalVentaPeces;
    }

    public bool GetEstadoProduccion()
    {
        return this.estadoProduccion;
    }

    public bool GetEntradaRacionNula()
    {
        return this.entradaRacionNula;
    }

    // --- Metodos Setter ---
    private void SetInventarioPeces(int peces)
    {
        this.inventarioPeces = peces;
    }

    public void SetDineroDisponible(double saldo)
    {
        this.dineroDisponible = saldo;
    }

    public bool GetGuardandoPartida()
    {
        return this.guardandoPartida;
    }

    // --- METODOS PARA MANEJO DE ESCENAS ----

    public void ReiniciarNivel()
    {
        //Eliminar datos
        GestionBD.singleton.EliminarEstadoJuego();
        //Recargue la escena
        Scene nivelActual = SceneManager.GetActiveScene();
        Debug.Log("Reiniciando el " + nivelActual.name);
        Time.timeScale = 1;
        SceneManager.LoadScene(nivelActual.name, LoadSceneMode.Single);
    }

    public void VolverHome()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MenuPrincipal");
    }

    // --- METODOS PARA MANEJO DE DATOS ----

    //Función para cargar los datos del juego para continuar jugando
    private void CargarPartida()
    {
        StartCoroutine(CargarEstadoPartida());
    }

    private IEnumerator CargarEstadoPartida()
    {
        yield return new WaitForSeconds(1);
        //Aquí debe actualizar los datos del jugador, modelo y peces en pantalla junto a su tamaño de acuerdo al peso
        if (GestionBD.singleton.GetRegistroData())
        {
            GestionBD.singleton.registroGameData.CargarDatos(registroPartida);
            //Consultar de BD el último registro
            produccion.SetActive(true);
            yield return new WaitForSeconds(1);
            this.nombreJugador = registroPartida[0];
            this.sesionJugador = registroPartida[1];
            SetDineroDisponible(double.Parse(registroPartida[2]));
            SetInventarioPeces(int.Parse(registroPartida[3]));
            produccion.GetComponent<ModeloN2>().SetPecesenLoteP(int.Parse(registroPartida[3]));
            produccion.GetComponent<LotePecesController>().SetPezCount(int.Parse(registroPartida[3]));
            produccion.GetComponent<ModeloN2>().SetTiempoIteracion(int.Parse(registroPartida[5]));
            produccion.GetComponent<PlayTimer>().SetTotalDias(int.Parse(registroPartida[5]));
            this.estadoProduccion = bool.Parse(registroPartida[6]);

            string[] datosParaModelo = new string[produccion.GetComponent<ModeloN2>().datosModelo.Length];
            Array.Copy(registroPartida, 10, datosParaModelo, 0, datosParaModelo.Length);
            produccion.GetComponent<ModeloN2>().SetDatosModelo(datosParaModelo);

            produccion.GetComponent<PlayTimer>().FijarDuracionDia(GestionBD.singleton.GetTiempoIteracion());
            produccion.GetComponent<ModeloN2>().timer.CorrerTiempo();

            print("Actualizados los datos.--------------");
            mojarraIntro.SetActive(false);
            GestionBD.singleton.SetRegistroData(false);
        }
        else
        {
            yield return new WaitForSeconds(4);
            IniciarProduccion(1);
            this.inventarioPeces = 1;
            mojarraIntro.SetActive(false);
        }
    }


    //Función para guardar el estado del juego
    public void SalvarEstado()
    {
        StartCoroutine(GuardarPartida());
    }

    private IEnumerator GuardarPartida()
    {
        guardandoPartida = true;
        GenerarDatosPartida();
        gameData.AsignarValores(this.datosPartida);
        string json = JsonUtility.ToJson(gameData);
        print(json);
        GestionBD.singleton.GuardarEstadoJuego(json);
        print("Guardado de datos realizado con éxito" + Time.time);
        yield return new WaitForSeconds(1);
        guardandoPartida = false;
    }

    //Función para guardar los datos de la partida
    private void GenerarDatosPartida()
    {
        /*Estructura de partida
        * [0] User
        * [1] Sesion
        * [2] Dinero
        * [3] Inventario peces
        * [4] Inventario alimento
        * [5] Dias transcurridos
        * [6] Estado produccion
        * [7] Tiempo iteracion
        * [8] Turno de venta
        * [9] Unión a gremio
        * [10-29] Datos del modelo
        */

        datosPartida[0] = nombreJugador;
        datosPartida[1] = sesionJugador;
        datosPartida[2] = this.dineroDisponible.ToString();
        datosPartida[3] = this.inventarioPeces.ToString();
        datosPartida[4] = 0.ToString();
        datosPartida[5] = (produccion.GetComponent<ModeloN2>().GetTiempoIteracion()).ToString();
        datosPartida[6] = this.estadoProduccion.ToString();
        datosPartida[7] = "libre";
        datosPartida[8] = 0.ToString();
        datosPartida[9] = unidoGremio.ToString();
        if (this.estadoProduccion)
        {
            for (int i = 0; i < produccion.GetComponent<ModeloN2>().datosModelo.Length; i++)
            {
                datosPartida[10 + i] = produccion.GetComponent<ModeloN2>().datosModelo[i];
            }
        }
        else
        {
            for (int j = 10; j < 30; j++)
            {
                datosPartida[j] = 0.ToString();
            }
        }
    }

    public void SalvarVenta()
    {
        StartCoroutine(GuardarVenta());
    }

    private IEnumerator GuardarVenta()
    {
        guardandoPartida = true;
        GenerarDatosVenta();
        ventaData.AsignarValores(this.datosVenta);
        string jsonventa = JsonUtility.ToJson(ventaData);
        print(jsonventa);
        GestionBD.singleton.GuardarVenta(jsonventa);
        if (GestionBD.singleton.GetEstadoVenta())
        {
            print("Guardado de venta realizado con éxito" + Time.time);
        }
        yield return new WaitForSeconds(1);
        guardandoPartida = false;
    }

    //Función para guardar los datos de venta
    private void GenerarDatosVenta()
    {
        /*Estructura de venta
         * [0] User
         * [1] Sesion
         * [2] Peso
         * [3] Número de peces 
         * [4] Valor de la venta
         * [5] Modalidad de venta
         * [6] Costo acumulado
         */
        datosVenta[0] = nombreJugador;
        datosVenta[1] = sesionJugador;
        datosVenta[2] = produccion.GetComponent<ModeloN2>().GetPesoPromedio().ToString();
        datosVenta[3] = pecesAVender.ToString();
        datosVenta[4] = totalVentaPeces.ToString();
        datosVenta[5] = "libre";
        datosVenta[6] = produccion.GetComponent<ModeloN2>().GetCostoProduccion().ToString();
    }

    //Función para consultar el precio del mercado
    public void ConsultarPrecio()
    {
        StartCoroutine(PrecioMercado());
        print("Consultando precio del mercado...");
    }

    private IEnumerator PrecioMercado()
    {
        while (true)
        {
            GestionBD.singleton.ConsultarDatosMercado();
            yield return new WaitForSeconds(1f);
            this.precioVentaPez = GestionBD.singleton.GetPrecioMercado();
            yield return new WaitForSeconds(tiempoMercado);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameLogicN3 : MonoBehaviour
{

    /*En este sccript se colocan todas las acciones lógicas que producen un cambio en el juego
    Efectos de pulsar los botones*/
    public bool isPaused = false;
    public bool estadoProduccion = false;

    //Variables de control de errores
    private bool errorDinero = false;
    private bool errorPeces = false;
    private bool errorAlimento = false;
    private bool errorPeso = false;
    private bool errorLote = false;
    private bool errorConexion = false;
    private int LIMITEPECES;
    [SerializeField]
    private bool guardandoPartida = false;
    [SerializeField]
    [Tooltip("Control para saber si hay datos registrados en la BD")]
    public string[] registroPartida;
    [Tooltip("Formato para registro de datos de partida en la BD")]
    public string[] datosPartida;
    [Tooltip("Formato para registro de venta en la BD")]
    public string[] datosVenta;
    public DatosJuego gameData = new DatosJuego();
    public DatosVenta ventaData = new DatosVenta();

    //Variables de operación para el juego
    [SerializeField]
    private int pecesAComprar;
    private double gramosAlimentoAComprar;
    [SerializeField]
    private float gramosAlimentoASuministrar;
    private int pecesAVender;
    [SerializeField]
    private double totalCompraPeces;
    private double totalCompraAlimento;
    private double totalVentaPeces;
    public GameObject produccion;
    public GameObject vendedorMercado;

    //Datos del jugador
    [SerializeField]
    private string nombreJugador;
    private string sesionJugador;
    private double dineroDisponible;
    private int inventarioPeces;
    private double inventarioAlimento;
    private bool unidoGremio = false;

    //Precios del nivel
    private int VALORUNITARIOPEZ;
    [SerializeField]
    private double precioVentaPez;
    private float PESOMINIMO;

    //Datos del servidor
    private int tiempoMercado;

    //Elementos UI necesarios para capturar los valores de operación y calcular totales
    //Compra de peces
    public InputField entradaCompraPeces;
    public Text PIValorTotalPeces;
    //Suministro de ración
    public InputField entradaRacion;
    //Venta de peces
    public InputField entradaVentaPeces;
    public Text PMValorTotalVenta;

    //Variables de control sobre entradas nulas o no válidas
    private bool entradaCompraPecesNula = true;
    private bool entradaRacionNula = true;
    private bool entradaVentaPecesNula = true;

    private void Awake()
    {
        VALORUNITARIOPEZ = 150;
        LIMITEPECES = 10000;
        PESOMINIMO = 0.125f;
        precioVentaPez = 8800;
        dineroDisponible = 100000;
        inventarioAlimento = 1000000;
    }

    // Use this for initialization
    void Start()
    {
        isPaused = false;
        CalcularValorCompraPeces();
        CalcularValorSuministroRacion();
        CalcularValorVentaPeces();

        nombreJugador = GestionBD.singleton.GetNombreJugador();
        sesionJugador = GestionBD.singleton.GetNombreSesion();
        tiempoMercado = GestionBD.singleton.GetTiempoMercado();

        datosPartida = new string[30];
        registroPartida = new string[30];
        datosVenta = new string[7];
        //produccion.GetComponent<PlayTimer>().FijarDuracionDia(GestionBD.singleton.GetTiempoIteracion());
        produccion.SetActive(false);
        //Traer datos de la BD de datos en caso de que existan
        CargarPartida();
        ConsultarPrecio();
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(this.inventarioPeces, this);
        //La lógica del juego debe estar mirando si hay una producción activa.
        estadoProduccion = produccion.GetComponent<ModeloN3>().GetProduccionActiva();
        if (Input.GetKeyDown("p"))
        {
            Pause();
        }
    }

    //Función para Pausar
    public void Pause()
    {
        if (isPaused == true)//¿El juego está pausado?
        {//Hagalo correr
            Time.timeScale = 1;
            isPaused = false;
        }
        else if (isPaused == false)
        {//Hagalo pausar
            Time.timeScale = 0;
            isPaused = true;
        }
    }

    //Método para efectuar compra de peces, calcula si hay dinero para la transacción y toma el número de peces a comprar para realizar la compra
    public void ComprarPeces()
    //Si hay produccion activa no puedo comprar;
    {
        bool excesoPeces = (pecesAComprar > LIMITEPECES) ? true : false;//Limite de peces que caben en el estanque
        if (!excesoPeces)
        {
            double subtotal = dineroDisponible - totalCompraPeces;
            if (subtotal >= 0)
            {
                //Modificación de los inventarios
                SetDineroDisponible(subtotal);
                SetInventarioPeces(pecesAComprar);
                Debug.Log("Compra de peces efectuada a los " + Time.time + ".");
                IniciarProduccion(pecesAComprar, totalCompraPeces);
                SalvarEstado();
            }
            else
            {
                //Ventana no alcanza el dinero
                errorDinero = true;
                //print("No le alcanza el dinero para comprar los peces");
            }
        }
        else
        {
            //Ventana No puede comprar más de esos peces
            errorLote = true;
        }
        entradaCompraPeces.text = null;
    }

    //Método que calcula el valor a pagar por un determinado número de peces, toma el contenido del input y multiplica por el precio del pez
    public void CalcularValorCompraPeces()
    {
        if (entradaCompraPeces.text != "" && !(entradaCompraPeces.text.Contains("-")))
        {
            int.TryParse(entradaCompraPeces.text, out pecesAComprar);
            if (pecesAComprar > 0)
            {
                totalCompraPeces = VALORUNITARIOPEZ * pecesAComprar;
                PIValorTotalPeces.text = "$ " + totalCompraPeces.ToString("N0");
                entradaCompraPecesNula = false;
            }
        }
        else
        {
            entradaCompraPecesNula = true;
            PIValorTotalPeces.text = "Valor a pagar";
        }
    }

    //Método para cambiar la ración de alimento que se suministra al estanque
    public void CambiarRacion()
    {
        //double subtotal = dineroDisponible - totalCompraAlimento;
        double racionLote = gramosAlimentoASuministrar * inventarioPeces;
        double nuevoInventarioAlimento = inventarioAlimento - racionLote;
        if ((nuevoInventarioAlimento) >= 0)
        {
            //Modificación de los inventarios
            //SetDineroDisponible(subtotal);
            //SetInventarioAlimento(nuevoInventarioAlimento);
            CambiarRacionAlimenticia(gramosAlimentoASuministrar);
            Debug.Log("Modificación de la ración de alimento efectuada a los " + Time.time + ".");
            SalvarEstado();
        }
        else
        {
            //Ventana no alcanza el alimento
            errorAlimento = true;
            //print("No le alcanza el inventario de alimento para suministrar la ración.");
        }
        entradaRacion.text = null;
    }

    public void CalcularValorSuministroRacion()
    {
        if (entradaRacion.text != "" && !(entradaRacion.text.Contains("-")))
        {
            float.TryParse(entradaRacion.text, out gramosAlimentoASuministrar);
            entradaRacionNula = false;
        }
        else
        {
            entradaRacionNula = true;
        }
    }
    //Método para efectuar venta de peces, valida el inventario de peces y realiza operaciones dependiendo si es una venta parcial (una parte del lote) o total del lote.
    public void VenderPeces()
    {
        //Guardo los datos de venta y luego acabo el modelo y los peces en el estanque
        //Reinicio el tiempo
        if ((float)PesoPez() - PESOMINIMO > 0.01f)
        {
            if (inventarioPeces >= pecesAVender)
            {
                double subtotal = dineroDisponible + totalVentaPeces;
                int nuevoInventarioPeces = inventarioPeces - pecesAVender;
                //¿Es una venta parcial?
                if (pecesAVender != inventarioPeces)
                {
                    SalvarVenta();
                    //Modificación de inventarios
                    SetInventarioPeces(nuevoInventarioPeces);
                    SetDineroDisponible(subtotal);
                    //Actualización de inventario de peces en el modelo
                    ModificarProduccion(inventarioPeces);
                    Debug.Log("Venta parcial realizada a los " + Time.time + ".");
                    vendedorMercado.GetComponent<Animator>().SetTrigger("venta");
                    SalvarEstado();
                }
                else if (pecesAVender == inventarioPeces)//¿Es una venta total?
                {
                    SalvarVenta();
                    //Modificación de inventarios
                    SetInventarioPeces(nuevoInventarioPeces);
                    SetDineroDisponible(subtotal);
                    //Actualización de datos en el modelo
                    FinalizarProduccion(nuevoInventarioPeces);
                    estadoProduccion = false;
                    Debug.Log("Venta total realizada a los " + Time.time + ".");
                    vendedorMercado.GetComponent<Animator>().SetTrigger("venta");
                    SalvarEstado();
                }
            }
            else
            {
                //Ventana no alcanza los peces del inventario
                errorPeces = true;
                print("No le alcanzan los peces");
            }
        }
        else
        {
            errorPeso = true;
            print("Los peces no tienen el peso mínimo");
        }

        entradaVentaPeces.text = null;
    }

    //Función que calcula el valor a pagar por un determinado número de peces, toma el contenido del input y multiplica por el precio del pez
    public void CalcularValorVentaPeces()
    {
        if (entradaVentaPeces.text != "")
        {
            //peces = Int32.Parse(entradaCompraPeces.text);
            int.TryParse(entradaVentaPeces.text, out pecesAVender);
            if (pecesAVender > 0)
            {
                double biomasaVenta = pecesAVender * PesoPez();
                totalVentaPeces = precioVentaPez * biomasaVenta;
                PMValorTotalVenta.text = biomasaVenta.ToString() + " kg \n" + "$ " + totalVentaPeces.ToString("N0");
                entradaVentaPecesNula = false;
            }
        }
        else
        {
            entradaVentaPecesNula = true;
            PMValorTotalVenta.text = "Total de la venta";
        }
    }
    // ---MÉTODOS DE MODIFICACIÓN DEL MODELO DE PRODUCCIÓN---
    private void IniciarProduccion(int peces, double costoInicial)
    {
        produccion.SetActive(true);
        produccion.GetComponent<PlayTimer>().FijarDuracionDia(GestionBD.singleton.GetTiempoIteracion());
        //Activación de una nueva producción
        produccion.GetComponent<ModeloN3>().timer.CorrerTiempo();
        produccion.GetComponent<ModeloN3>().SetCostoInicialPeces(costoInicial);
        produccion.GetComponent<ModeloN3>().SetCostosTotales(costoInicial);
        produccion.GetComponent<ModeloN3>().SetProduccionActiva(true);
        produccion.GetComponent<ModeloN3>().SetInventarioInicialPeces(peces);
        produccion.GetComponent<ModeloN3>().SetPecesenLoteP(peces);
        //produccion.GetComponent<LotePecesController>().SetPezCount(peces); 
        CalcularEscalaAlimento(0);
    }

    private void ModificarProduccion(int peces)
    {
        //Modificación de la producción activa
        produccion.GetComponent<ModeloN3>().SetPecesenLoteP(peces);
        //produccion.GetComponent<LotePecesController>().SetPezCount(peces);
        produccion.GetComponent<ModeloN3>().SumarVenta(totalVentaPeces, pecesAVender);
    }

    private void FinalizarProduccion(int peces)
    {
        //Finalización de la producción activa
        produccion.GetComponent<ModeloN3>().SetProduccionActiva(false);
        produccion.GetComponent<ModeloN3>().SetPecesenLoteP(peces);
        //produccion.GetComponent<LotePecesController>().SetPezCount(peces);
        produccion.GetComponent<ModeloN3>().SumarVenta(totalVentaPeces, pecesAVender);
        //produccion.SetActive(false);
        produccion.GetComponent<ModeloN3>().SetResumenProduccion(true);
    }

    private void CambiarRacionAlimenticia(double nuevaracion)
    {
        produccion.GetComponent<ModeloN3>().SetRacionAlimenticiaP(nuevaracion);
        CalcularEscalaAlimento(nuevaracion);
    }

    //Función para ajustar visualmente el tamaño de la ración por medio de la escala del GameObject
    private void CalcularEscalaAlimento(double racionActual)
    {
        if (racionActual == 0)
        {
            produccion.GetComponent<LotePecesController>().goal.GetComponent<MeshRenderer>().enabled = false;
        }
        else
        {
            produccion.GetComponent<LotePecesController>().goal.GetComponent<MeshRenderer>().enabled = true;
            if (racionActual <= 1)
            {
                produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            }
            else
            {
                if (racionActual <= 2)
                {
                    produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                }
                else
                {
                    if (racionActual <= 3)
                    {
                        produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                    }
                    else
                    {
                        if (racionActual <= 4)
                        {
                            produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                        }
                        else
                        {
                            produccion.GetComponent<LotePecesController>().goal.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                        }
                    }
                }
            }
        }

    }

    //Método para ajustar la escala de gramos
    private string AjustarGramos(double valor)
    {
        string mensaje = "";
        if (valor < 1000)
        {
            mensaje = Math.Round(valor, 2).ToString() + " g";
        }
        else if (valor >= 1000 && valor < 1000000)
        {
            mensaje = Math.Round(valor / 1000f, 3).ToString() + " kg";
        }
        else if (valor >= 1000000)
        {
            mensaje = Math.Round(valor / 1000000f, 3).ToString() + " t";
        }
        return mensaje;
    }


    //Método para convertir el peso promedio del pez en kg
    private double PesoPez()
    {
        double peso = produccion.GetComponent<ModeloN3>().GetPesoPromedio();
        return Math.Round((peso / 1000), 3);
    }

    // --- METODOS GETER AND SETER ----

    // --- Metodos Getter ---

    public int GetInventarioPeces()
    {
        return this.inventarioPeces;
    }

    public double GetInventarioAlimento()
    {
        return this.inventarioAlimento;
    }

    public double GetDineroDisponible()
    {
        return this.dineroDisponible;
    }

    public int GetValorUnitarioPez()
    {
        return this.VALORUNITARIOPEZ;
    }

    public double GetPrecioVentaPez()
    {
        return this.precioVentaPez;
    }

    public bool GetErrorDinero()
    {
        return this.errorDinero;
    }

    public bool GetErrorLote()
    {
        return this.errorLote;
    }

    public bool GetErrorAlimento()
    {
        return this.errorAlimento;
    }

    public bool GetErrorPeces()
    {
        return this.errorPeces;
    }

    public bool GetErrorPeso()
    {
        return this.errorPeso;
    }

    public bool GetErrorConexion()
    {
        return this.errorConexion;
    }

    public bool GetEntradaCompraPecesNula()
    {
        return this.entradaCompraPecesNula;
    }

    public bool GetEntradaRacionNula()
    {
        return this.entradaRacionNula;
    }

    public bool GetEntradaVentaPecesNula()
    {
        return this.entradaVentaPecesNula;
    }

    public bool GetEstadoProduccion()
    {
        return this.estadoProduccion;
    }

    public bool GetGuardandoPartida()
    {
        return this.guardandoPartida;
    }

    // --- Metodos Setter ---
    private void SetInventarioPeces(int peces)
    {
        this.inventarioPeces = peces;
        produccion.GetComponent<LotePecesController>().SetPezCount(peces);
    }

    public void SetInventarioAlimento(double gramos)
    {
        this.inventarioAlimento = gramos;
    }

    public void SetDineroDisponible(double saldo)
    {
        this.dineroDisponible = saldo;
    }

    public void SetErrorDinero(bool estado)
    {
        this.errorDinero = estado;
    }

    public void SetErrorLote(bool estado)
    {
        this.errorLote = estado;
    }

    public void SetErrorPeces(bool estado)
    {
        this.errorPeces = estado;
    }

    public void SetErrorAlimento(bool estado)
    {
        this.errorAlimento = estado;
    }

    public void SetErrorPeso(bool estado)
    {
        this.errorPeso = estado;
    }

    public void SetErrorConexion(bool estado)
    {
        this.errorConexion = estado;
    }

    // --- METODOS PARA MANEJO DE ESCENAS ----

    public void ReiniciarNivel()
    {
        //Eliminar datos
        GestionBD.singleton.EliminarEstadoJuego();
        //Recargue la escena
        Scene nivelActual = SceneManager.GetActiveScene();
        Debug.Log("Reiniciando el " + nivelActual.name);
        Time.timeScale = 1;
        SceneManager.LoadScene(nivelActual.name, LoadSceneMode.Single);
    }

    public void VolverHome()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MenuPrincipal");
    }

    // --- METODOS PARA MANEJO DE DATOS ----

    //Función para cargar los datos del juego para continuar jugando
    private void CargarPartida()
    {
        StartCoroutine(CargarEstadoPartida());
    }

    private IEnumerator CargarEstadoPartida()
    {
        Debug.Log("We got here", this);
        yield return new WaitForSeconds(1);
        Debug.Log("We pass that", this);
        //Aquí debe actualizar los datos del jugador, modelo y peces en pantalla junto a su tamaño de acuerdo al peso
        Debug.Log("GetRegistroData: "+GestionBD.singleton.GetRegistroData(), this);
        if (GestionBD.singleton.GetRegistroData())
        {
            GestionBD.singleton.registroGameData.CargarDatos(registroPartida);
            //Consultar de BD el último registro
            produccion.SetActive(true);
            yield return new WaitForSeconds(1);
            this.nombreJugador = registroPartida[0];
            this.sesionJugador = registroPartida[1];
            SetDineroDisponible(double.Parse(registroPartida[2]));
            SetInventarioPeces(int.Parse(registroPartida[3]));
            produccion.GetComponent<ModeloN3>().SetPecesenLoteP(int.Parse(registroPartida[3]));
            //produccion.GetComponent<LotePecesController>().SetPezCount(int.Parse(registroPartida[3]));
            Debug.Log("Inventario Peces Texto: " + registroPartida[4], this);
            SetInventarioAlimento(double.Parse(registroPartida[4]));
            produccion.GetComponent<ModeloN3>().SetTiempoIteracion(int.Parse(registroPartida[5]));
            produccion.GetComponent<PlayTimer>().SetTotalDias(int.Parse(registroPartida[5]));
            this.estadoProduccion = bool.Parse(registroPartida[6]);

            string[] datosParaModelo = new string[produccion.GetComponent<ModeloN3>().datosModelo.Length];
            Array.Copy(registroPartida, 10, datosParaModelo, 0, datosParaModelo.Length);
            produccion.GetComponent<ModeloN3>().SetDatosModelo(datosParaModelo);

            produccion.GetComponent<PlayTimer>().FijarDuracionDia(GestionBD.singleton.GetTiempoIteracion());
            produccion.GetComponent<ModeloN3>().timer.CorrerTiempo();

            print("Actualizados los datos.--------------");
            
            GestionBD.singleton.SetRegistroData(false);
        }
    }

    //Función para guardar el estado del juego
    public void SalvarEstado()
    {
        StartCoroutine(GuardarPartida());
    }

    private IEnumerator GuardarPartida()
    {
        guardandoPartida = true;
        GenerarDatosPartida();
        gameData.AsignarValores(this.datosPartida);
        string json = JsonUtility.ToJson(gameData);
        print(json);
        GestionBD.singleton.GuardarEstadoJuego(json);
        print("Guardado de datos realizado con éxito" + Time.time);
        yield return new WaitForSeconds(1);
        guardandoPartida = false;
    }

    //Función para guardar los datos de la partida
    private void GenerarDatosPartida()
    {
        /*Estructura de partida
         * [0] User
         * [1] Sesion
         * [2] Dinero
         * [3] Inventario peces
         * [4] Inventario alimento
         * [5] Tiempo iteracion
         * [6] Estado produccion
         * [7] Modalidad de venta
         * [8] Turno de venta
         * [9] Unión a gremio
         * [10-29] Datos del modelo
         */
        datosPartida[0] = nombreJugador;
        datosPartida[1] = sesionJugador;
        datosPartida[2] = dineroDisponible.ToString();
        datosPartida[3] = inventarioPeces.ToString();
        datosPartida[4] = inventarioAlimento.ToString();
        datosPartida[5] = (produccion.GetComponent<ModeloN3>().GetTiempoIteracion()).ToString();
        datosPartida[6] = estadoProduccion.ToString();
        datosPartida[7] = "libre";
        datosPartida[8] = 0.ToString();
        datosPartida[9] = unidoGremio.ToString();
        if (this.estadoProduccion)
        {
            for (int i = 0; i < produccion.GetComponent<ModeloN3>().datosModelo.Length; i++)
            {
                datosPartida[10 + i] = produccion.GetComponent<ModeloN3>().datosModelo[i];
            }
        }
        else
        {
            for (int j = 10; j < 30; j++)
            {
                datosPartida[j] = 0.ToString();
            }
        }
    }

    public void SalvarVenta()
    {
        StartCoroutine(GuardarVenta());
    }

    private IEnumerator GuardarVenta()
    {
        guardandoPartida = true;
        GenerarDatosVenta();
        ventaData.AsignarValores(this.datosVenta);
        string jsonventa = JsonUtility.ToJson(ventaData);
        print(jsonventa);
        GestionBD.singleton.GuardarVenta(jsonventa);
        if (GestionBD.singleton.GetEstadoVenta())
        {
            print("Guardado de venta realizado con éxito" + Time.time);
        }
        yield return new WaitForSeconds(1);
        guardandoPartida = false;
    }

    //Función para guardar los datos de venta
    private void GenerarDatosVenta()
    {
        /*Estructura de venta
         * [0] User
         * [1] Sesion
         * [2] Peso
         * [3] Número de peces 
         * [4] Valor de la venta
         * [5] Modalidad de venta
         * [6] Costo acumulado
         */
        datosVenta[0] = nombreJugador;
        datosVenta[1] = sesionJugador;
        datosVenta[2] = produccion.GetComponent<ModeloN3>().GetPesoPromedio().ToString();
        datosVenta[3] = pecesAVender.ToString();
        datosVenta[4] = totalVentaPeces.ToString();
        datosVenta[5] = "libre";
        datosVenta[6] = produccion.GetComponent<ModeloN3>().GetCostosTotales().ToString();
    }

    //Función para consultar el precio del mercado
    public void ConsultarPrecio()
    {
        StartCoroutine(PrecioMercado());
        print("Consultando precio del mercado...");
    }

    private IEnumerator PrecioMercado()
    {
        while (true)
        {
            GestionBD.singleton.ConsultarDatosMercado();
            yield return new WaitForSeconds(1f);
            this.precioVentaPez = GestionBD.singleton.GetPrecioMercado();
            yield return new WaitForSeconds(tiempoMercado);
        }
    }

}

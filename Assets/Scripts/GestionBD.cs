﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;


public class GestionBD : MonoBehaviour {

    public bool errorEnConexion;

    //Datos jugador
    [SerializeField]
    private string nombreJugador;
    [SerializeField]
    private string sesionActual;
    [SerializeField]
    private int nivelActual;
    [SerializeField]
    private int tiempoIteracionActual;
    [SerializeField]
    private int tiempoMercadoActual;
    [SerializeField]
    private string[] datosOferta = new string[7];
    [SerializeField]
    public DatosJuego registroGameData = new DatosJuego();

    [SerializeField]
    private bool sesionIniciada = false;
    private bool registroOferta = false;
    [SerializeField]
    private bool registroData = false;

    [SerializeField]
    private bool guardadoVenta = false;

    [SerializeField]
    private string[] sesiones;
    private string[] jugadoresPorTurnos;

    //Datos mercado
    [SerializeField]
    private double precioActualMercado;
    [SerializeField]
    private int turnoActualServidor;

    //Datos para conexión
    //private string urlServidor = "/pesco/pruebajuego/";
    private string urlServidor = "http://localhost:8000/pruebajuego/";
    //private string urlServidor = "http://simon.uis.edu.co:8081/proyectomoviles/mercado";
    [SerializeField]
    private string url;

    public static GestionBD singleton;

    ///Respuestas WEB
    ///   400 = No establece conexión
    ///   401 = No encontró datos
    ///   402 = El usuario ya existe
    ///   403 = No hay sesiones vinculadas
    ///   404 = No hay ofertas en el gremio
    ///   405 = No hay un precio de mercado
    ///   406 = No hay jugadores en modalidad de turnos
    ///   
    ///   200 = Datos encontrados
    ///   201 = Usuario registrado
    ///   202 = Score actualizado
    ///   204 = Venta registrada
    ///   205 = Usuario registrado en el gremio
    ///   206 = Registro de Level Win
    ///   
    ///   101 = Datos eliminados
    ///   102 = Usuario eliminado del gremio


    private void Awake()
    {
        if (singleton == null)
        {
            singleton = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    public void IniciarSesion(string us, string pw)
    {
        StartCoroutine(Login(us, pw));
    }

    public void ConsultarOfertaGremio()
    {
        StartCoroutine(Ofertas(sesionActual));
    }

    public void ConsultarDatosBase()
    {
        StartCoroutine(Datos(nombreJugador,sesionActual));
    }

    public void GuardarEstadoJuego(string datos)
    {
        StartCoroutine(GuardarDatosJuego(nombreJugador, sesionActual, datos));
    }

    public void EliminarEstadoJuego()
    {
        StartCoroutine(EliminarDatosJuego(nombreJugador, sesionActual));
    }

    public void ConsultarDatosMercado()
    {
        StartCoroutine(PrecioMercado(sesionActual));
    }

    public void GuardarVenta(string datosVenta)
    {
        StartCoroutine(AgregarVenta(nombreJugador, sesionActual, datosVenta));
    }

    public void ConsultarEstadoTurnos()
    {
        StartCoroutine(ConsultarTurnos(sesionActual));
    }

    public void VincularGremio()
    {
         StartCoroutine(AgregarOferenteGremio(nombreJugador, sesionActual));
    }

    public void DesvincularGremio()
    {
        StartCoroutine(EliminarOferenteGremio(nombreJugador, sesionActual));
    }

    public void RegistrarFinalizacionNivel()
    {
        StartCoroutine(RegistrarLevelWin(nombreJugador, sesionActual));
    }

    public void RegistrarUsuario(string user, string password)
    {
        StartCoroutine(Registrar(user,password));
    }

    IEnumerator Login(string us, string pw)
    {
        url = urlServidor + "login";
        WWWForm form = new WWWForm();
        Debug.Log(url, this);
        form.AddField("usernamePost", us);
        form.AddField("passwordPost", pw);
        UnityWebRequest conexion = UnityWebRequest.Post(url, form);
        //WWW conexion = new WWW("http://localhost/pruebajuego/login?uss=" + txtUsuario.text + "&pss=" + txtcontraseña.text);
        yield return conexion.SendWebRequest();
        //Debug.Log(conexion.text);
        if (conexion.responseCode == 200)
        {
            print("El usuario sí existe");
            nombreJugador = us;
            sesionIniciada = true;
            //StartCoroutine(Datos(us));
            SceneManager.LoadScene("MenuPrincipal");
            StartCoroutine(Sesiones(nombreJugador));
        }
        else
        {
            if (conexion.responseCode == 401)
            {
                print("Usuario o contraseña incorrectos.");
                SetEstadoError(true);
            }
            else
            {
                //print(conexion.text);
                print("Error en la conexión con la base de datos");
                SetEstadoError(true);
            }
        }
    }

    IEnumerator Sesiones(string us)
    {
        url = urlServidor + "sesiones";
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", us);

        UnityWebRequest conexion = UnityWebRequest.Post(url, form);
        yield return conexion.SendWebRequest();

        if (conexion.responseCode == 402)
        {
            print("No hay sesiones vinculadas.");
        }
        else
        {
            string[] nDatos = conexion.downloadHandler.text.Split('|');
            sesiones = new string[nDatos.Length - 1];
            foreach (var item in nDatos)
            {

                print(item.ToString());
            }
            for (int i = 0; i < sesiones.Length; i++)
            {
                sesiones[i] = nDatos[i];
            }
        }
    }

    IEnumerator Ofertas(string se)
    {
        url = urlServidor + "ofertas";
        WWWForm form = new WWWForm();
        form.AddField("sesionPost", se);

        UnityWebRequest conexion = UnityWebRequest.Post(url, form);
        yield return conexion.SendWebRequest();

        if (conexion.responseCode == 403)
        {
            print("No hay ofertas disponibles.");
        }
        else
        {

            string[] nDatosOferta = conexion.downloadHandler.text.Split('&');
            if (nDatosOferta.Length != 7)
            {
                //print("Error en la oferta");
            }
            else
            {
                for (int i = 0; i < nDatosOferta.Length; i++)
                {
                    datosOferta[i] = nDatosOferta[i];
                }
                registroOferta = true;
            }
        }
    }

    IEnumerator Datos(string us, string se)
    {

        url = urlServidor + "loaddata";
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", us);
        form.AddField("sesionPost", se);

        UnityWebRequest conexion = UnityWebRequest.Post(url, form);
        yield return conexion.SendWebRequest();
        if (conexion.responseCode == 400)
        {
            print("No hay conexión.");
            SetEstadoError(true);
        }
        else
        {
            if (conexion.responseCode == 401)
            {
                print("No hay datos.");
                registroData = false;
                //se crea nuevos datos
            }
            else
            {
                Debug.Log(conexion.downloadHandler.text, this);
                JsonUtility.FromJsonOverwrite(conexion.downloadHandler.text, registroGameData);
                registroData = true;
                print("Existe datos de juego en la base.");
            }
        }
    }

    IEnumerator GuardarDatosJuego(string us, string se, string data)
    {
        url = urlServidor + "savedata";
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", us);
        form.AddField("sesionPost", se);
        form.AddField("dataPost", data);
        UnityWebRequest conexion = UnityWebRequest.Post(url, form);
        yield return conexion.SendWebRequest();
        print(conexion.responseCode);
    }

    IEnumerator EliminarDatosJuego(string us, string se)
    {
        url = urlServidor + "deletedata";
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", us);
        form.AddField("sesionPost", se);

        UnityWebRequest conexion = UnityWebRequest.Post(url, form);
        yield return conexion.SendWebRequest();

        if (conexion.responseCode == 400)
        {
            print("No hay conexión.");
            SetEstadoError(true);
        }
        else
        {
            if (conexion.responseCode == 401)
            {
                print("No hay datos.");
            }
            else
            {
                print("Datos eliminados");
            }
        }
    }

    IEnumerator PrecioMercado(string se)
    {
        url = urlServidor + "mercado";
        WWWForm form = new WWWForm();
        form.AddField("sesionPost", se);

        UnityWebRequest conexion = UnityWebRequest.Post(url, form);
        yield return conexion.SendWebRequest();

        if (conexion.responseCode == 405)
        {
            print("No hay un precio de mercado.");
        }
        else
        {

            string[] nDatosMercado = conexion.downloadHandler.text.Split('|');
            if (nDatosMercado.Length != 2)
            {
                //print("Error en la consulta de los datos del mercado");
            }
            else
            {
                precioActualMercado = double.Parse(nDatosMercado[0]);
                turnoActualServidor = int.Parse(nDatosMercado[1]);
            }
        }
    }

    IEnumerator AgregarVenta(string us, string se, string data)
    {
        url = urlServidor + "saveventa";
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", us);
        form.AddField("sesionPost", se);
        form.AddField("dataPost", data);
        UnityWebRequest conexion = UnityWebRequest.Post(url, form);
        yield return conexion.SendWebRequest();
        
        guardadoVenta = false;
        print(conexion.responseCode);
        if (conexion.responseCode == 400)
        {
            print("No hay conexión.");
        }
        else
        {
            if (conexion.responseCode == 204)
            {
                print("Datos de venta guardados con éxito");
                guardadoVenta = true;
            }
        }
    }

    IEnumerator ConsultarTurnos(string se)
    {
        url = urlServidor + "turnos";
        WWWForm form = new WWWForm();
        form.AddField("sesionPost", se);
        UnityWebRequest conexion = UnityWebRequest.Post(url, form);
        yield return conexion.SendWebRequest();
        if (conexion.responseCode == 400)
        {
            print("No hay conexión");
        }
        else
        {
            if (conexion.responseCode == 402)
            {
                print("No hay jugadores con modalidad de turno");
            }
            else
            {
                string[] nDatosTurnos = conexion.downloadHandler.text.Split('|');
                jugadoresPorTurnos = new string[nDatosTurnos.Length - 1];
                foreach (var item in nDatosTurnos)
                {

                    print(item.ToString());
                }
                for (int i = 0; i < jugadoresPorTurnos.Length; i++)
                {
                    jugadoresPorTurnos[i] = nDatosTurnos[i];
                }
            }
        }
    }

    IEnumerator AgregarOferenteGremio(string us, string se)
    {
        url = urlServidor + "addintgremio";
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", us);
        form.AddField("sesionPost", se);
        UnityWebRequest conexion = UnityWebRequest.Post(url, form);
        yield return conexion.SendWebRequest();
        bool guardado = false;
        if (conexion.responseCode == 400)
        {
            print("No hay conexión");
        }
        else{
            if (conexion.responseCode == 205)
            {
                print("Ingreso a gremio guardado con éxito");
                guardado = true;
            }
        }
        yield return guardado;
    }

    IEnumerator EliminarOferenteGremio(string us, string se)
    {
        url = urlServidor + "deleteintgremio";
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", us);
        form.AddField("sesionPost", se);
        UnityWebRequest conexion = UnityWebRequest.Post(url, form);
        yield return conexion.SendWebRequest();
        if (conexion.responseCode == 400)
        {
            print("No hay conexión");
        }
        else
        {
            if (conexion.responseCode == 102)
            {
                print("Salida del gremio registrada con éxito");
            }
        }
        yield return null;
    }

    IEnumerator RegistrarLevelWin(string us, string se)
    {
        url = urlServidor + "savelevelwin";
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", us);
        form.AddField("sesionPost", se);
        UnityWebRequest conexion = UnityWebRequest.Post(url, form);
        yield return conexion.SendWebRequest();
        if (conexion.responseCode == 400)
        {
            print("No hay conexión");
        }
        else
        {
            if (conexion.responseCode == 206)
            {
                print("Registro de finalización de partida con éxito.");
            }
        }
        yield return null;
    }

    IEnumerator Registrar(string us, string pw)
    {
        url = urlServidor + "registro?uss=" + us + "&pss=" + pw;
        UnityWebRequest conexion = UnityWebRequest.Get(url);
        //WWW conexion = new WWW("http://localhost/pruebajuego/registro?uss=" + txtUsuario.text + "&pss=" + txtcontraseña.text);
        yield return conexion.SendWebRequest();
        if (conexion.responseCode == 402)
        {
            Debug.LogError("Usuario ya existe.");
        }
        else if (conexion.responseCode == 201)
            {
                //nombreUsuario = username.text;
                
                sesionIniciada = true;
            }else
            {
                Debug.LogError("No se pudo establecer la conexión con la base de datos.");
            }
    }

   

    //Métodos get
    public bool GetEstadoError()
    {
        return this.errorEnConexion;
    }

    public string GetNombreJugador()
    {
        return this.nombreJugador;
    }

    public string GetNombreSesion()
    {
        return this.sesionActual;
    }

    public bool GetEstadoSesion()
    {
        return this.sesionIniciada;
    }

    public bool GetEstadoVenta()
    {
        return this.guardadoVenta;
    }

    public int GetNivel()
    {
        return this.nivelActual;
    }

    public int GetTiempoIteracion()
    {
        return this.tiempoIteracionActual;
    }

    public int GetTiempoMercado()
    {
        return this.tiempoMercadoActual;
    }

    public double GetPrecioMercado()
    {
        return this.precioActualMercado;
    }

    public int GetTurnoMercado()
    {
        return this.turnoActualServidor;
    }

    public bool GetRegistroOferta()
    {
        return this.registroOferta;
    }

    public bool GetRegistroData()
    {
        return this.registroData;
    }

    public string[] GetSesiones()
    {
        return sesiones;
    }

    public string[] GetDatosOferta()
    {
        return datosOferta;
    }

    //Métodos set
    public void SetEstadoError(bool estado)
    {
        this.errorEnConexion = estado;
    }

    public void SetRegistroData(bool estado)
    {
        this.registroData = false;
    }

    public void SetSesionActual(string sesion)
    {
        this.sesionActual = sesion;
    }

    public void SetNivelActual(int nivel)
    {
        this.nivelActual = nivel;
    }

    public void SetTiempoIteracion(int tIteracion)
    {
        this.tiempoIteracionActual = tIteracion;
    }

    public void SetTiempoMercado(int tMercado)
    {
        this.tiempoMercadoActual = tMercado;
    }

    public void SetEstadoSesion(bool estado)
    {
        this.sesionIniciada = estado;
    }

}

﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class ModeloN5 : MonoBehaviour {

    //-->Variables de gestión del juego con Unity
    private bool produccionActiva = false;
    private bool resumenProduccion = false;
    //Campos de interacción del modelo
    public GameLogicN5 datosjuego;
    //Ayudas visuales
    public Image PesoLoadingBar;
    private float pesoControl = 500f;
    private bool controlBarra;
    public Image barraPeso;
    public Sprite barraPrimerRango;
    public Sprite barraSegundoRango;

    //Campos generales
    private int dato;
    public PlayTimer timer;
    [SerializeField]
    private int tiempoIteracion=0;
    private int inventarioInicialPeces = 0;
    private double totalAlimentoSuministrado;
    public int actSize = 1;
    public string[] datosModelo = new string[20];

    //Declaración parámetros (Propios de cada nivel)
    private double factorMaximoConversionP = 0.92;
    private double factorMaximoProduccionP = 0.7;
    private double pesoMaximoP = 1200;
    //El área del estanque es el área del espejo del agua por la profundidad, se utilizará un radio de 1.5 m x h=1 m
    private double AreaEstanque=50;
    private double tasaAireacion = 0.2;
    private double nivelDeseadoOxigeno = 5;
    private double gastoFijoDiario=2;
    private double costoAlimento;

    //-->Variables del modelo Pesco Pez
    private string sp = " ";
    //Variables
    private double racionAlimenticiaRequeridaP;
    private double racionAlimenticiaSuministradaP;
    private double consumoRealP;
    private double consumoMantenimiento;
    private double consumoProduccion;
    private double factorConversionP;
    [SerializeField]
    private double coberturaPesoMaximoP;
    private double fraccionMantenimiento;
    //  Flujos
    private double incrementoPesoPezP;
    // Niveles
    private double pesoPezP;

    //-->Sector de inventario de alimento
    //Variables
    private double racionLoteASuministrar;
    private double racionAlimenticiaASuministrarP;
    //Flujos
    private double racionAlimenticiaLote;
    //Niveles
    private double inventarioAlimento;

    //-->Sector de temperatura
    //Niveles
    private double temperatura;

    //-->Sector de estanque
    //Variables
    private double biomasa;
    //Flujos
    [SerializeField]
    private int muertePeces;
    //Niveles
    [SerializeField]
    private int pecesEnLoteP;
    private int pecesMuertos;

    //-->Sector de oxigeno del agua
    //Variables
    private double densidadDeGramosPeces;
    private double diferenciaOxigeno;
    //Flujos
    private double perdidaOxigenoPorDensidad;
    private double gananciaOxigeno;
    //Niveles
    private double oxigenoDisuelto;

    //-->Sector de costos de estanque
    //Variables
    [SerializeField]
    private double costoInicialPeces;
    private double costoMantenimientoDiario;
    private double costoTotalAlimento;
    private double gastoAlimento;
    
    //Flujos
    private double incrementoGastos;
    //Niveles
    [SerializeField]
    private double totalCostos;

    //-->Sector de ventas y utilidades
    //Variables
    [SerializeField]
    private int totalPecesVendidos;
    //Niveles
    [SerializeField]
    private double totalVentas;

    //Llamar información necesaria de otros scripts
    private void Awake()
    {
        timer = gameObject.GetComponent<PlayTimer>();
        pecesEnLoteP = 0;
        costoInicialPeces = 0.0;
        
    }

    //Método que se ejecuta cuando es llamado por primera vez el script, también cuando se ha puesto Enable el objeto.
    private void Start()
    {
        produccionActiva = true;
        StartCoroutine(ValoresPorDefecto());
        StartCoroutine(GetDatosModelo());
        produccionActiva = true;
        costoAlimento = datosjuego.GetValorUnitarioAlimento();
        //StartCoroutine(ActualizarSimulacion(tiempoIteracion));
        //timer = gameObject.AddComponent<PlayTimer>();
        controlBarra = true;
    }

    /*Aquí se colocan todas las funciones que requieran de actualizar en referencia 
    a eventos que han sucedido, en qué momento se hace cambios de simulación.*/
    void Update()
    {
        if (produccionActiva == true)
        {
            dato = timer.GetTotalDias();
            if (tiempoIteracion != dato)
            {
                //Debug.Log("Cambió el día");
                StartCoroutine(ActualizarSimulacion(dato - tiempoIteracion));
                StartCoroutine(GetDatosModelo());
                tiempoIteracion = dato;
                datosjuego.SalvarEstado();
                //Crecimiento de peces
                CrecerPeces();
                PesoLoadingBar.fillAmount = (float)(coberturaPesoMaximoP);
            }
        }
        else if (produccionActiva == false)
        {
            if (resumenProduccion == true)
            {
                timer.ReiniciarTiempo();
                //resumenProduccion = false;
            } 
        }
    }

    private void CrecerPeces()
    {
        gameObject.GetComponent<LotePecesController>().SetPezPeso(this.pesoPezP);
    }

    //Función para cambiar fondo de barra del peso
    private void CambiarRangoBarra()
    {
        if (controlBarra)
        {
            if (((float)pesoPezP - pesoControl) < 0.01f)
            {
                barraPeso.GetComponent<Image>().sprite = barraPrimerRango;
            }
            else
            {
                barraPeso.GetComponent<Image>().sprite = barraSegundoRango;
            }
            controlBarra = false;
        }
    }

    public void ReiniciarSimulacion()
    {
        tiempoIteracion = 0;
        StartCoroutine(ValoresPorDefecto());
        StartCoroutine(GetDatosModelo());
        resumenProduccion = false;
        //Debug.Log("Empezamos de cero...");
        controlBarra = true;
        CambiarRangoBarra();
    }

    IEnumerator ActualizarSimulacion(int iteraciones)
    {
        for (int i = 1; i <= iteraciones; i++)
        {
            StartCoroutine(IteracionModelo());
            tiempoIteracion = tiempoIteracion + iteraciones;
            //print(iteraciones);
        }
        yield return null;
    }

    /**
     * Reestablece los valores por defecto de las variables implicadas en la simulación
     */
    IEnumerator ValoresPorDefecto()
    {
        pesoPezP = 1;
        coberturaPesoMaximoP = pesoPezP/pesoMaximoP;
        factorConversionP = 0.0;
        consumoRealP = 0.0;
        temperatura = 23.0;
        racionAlimenticiaRequeridaP = pesoPezP * (PescoMultiplicadorEstanque.CalcTasaAlimentacion(pesoPezP) / 100) * PescoMultiplicadorEstanque.EfectTempAlim(temperatura);
        racionAlimenticiaSuministradaP = 0.0;
        incrementoPesoPezP = 0.0;
        fraccionMantenimiento = 0.0;
        consumoMantenimiento = 0.0;
        consumoProduccion = 0.0;
    
        biomasa = pesoPezP * pecesEnLoteP;
        muertePeces = 0;
        pecesMuertos = 0;

        oxigenoDisuelto = 4;
        densidadDeGramosPeces = biomasa / AreaEstanque;
        gananciaOxigeno = 0.0;
        perdidaOxigenoPorDensidad = 0.0;

        incrementoGastos = 0.0;
        gastoAlimento = 0.0;
        totalCostos = costoInicialPeces;
        costoMantenimientoDiario = 0.0;

        totalVentas = 0.0;
        totalPecesVendidos = 0;
        yield return null;
    }

    /**
     * Este método ejecuta las ecuaciones del modelo, representa una iteración, al
     * ser ejecutado varias veces representa las iteraciones del modelo
     */
    IEnumerator IteracionModelo()
    {
        //Sector temperatura
        //---<temperatura = Math.Round(UnityEngine.Random.Range(16.0f, 32.0f), 2);
        temperatura = Math.Round(UnityEngine.Random.Range(18.0f, 28.0f), 0);

        //Sector inventario alimento
        inventarioAlimento = datosjuego.GetInventarioAlimento();
        racionLoteASuministrar = racionAlimenticiaASuministrarP * pecesEnLoteP;
        racionAlimenticiaLote = Math.Min(inventarioAlimento,racionLoteASuministrar);
        inventarioAlimento = inventarioAlimento - racionAlimenticiaLote;
        datosjuego.SetInventarioAlimento(inventarioAlimento);
        //datosjuego.SetInventarioAlimento(datosjuego.GetInventarioAlimento() - alimentoLote);
        totalAlimentoSuministrado = totalAlimentoSuministrado + racionAlimenticiaLote;
              
        //Sector Crecimiento en peso
        coberturaPesoMaximoP = pesoPezP / pesoMaximoP;
        factorConversionP = factorMaximoConversionP * PescoMultiplicadorEstanque.MultiplicadorFactorConversion(coberturaPesoMaximoP);
        fraccionMantenimiento =1-(factorMaximoProduccionP* PescoMultiplicadorEstanque.MultiplicadorFactorConversion(coberturaPesoMaximoP));
        consumoMantenimiento = racionAlimenticiaRequeridaP * fraccionMantenimiento;
        racionAlimenticiaSuministradaP = (racionAlimenticiaLote / pecesEnLoteP);
        consumoRealP = Math.Min(racionAlimenticiaRequeridaP, racionAlimenticiaSuministradaP);
        consumoProduccion=(consumoRealP-consumoMantenimiento<0) ? 0.0 : consumoRealP - consumoMantenimiento;
        incrementoPesoPezP = consumoProduccion * factorConversionP;
        pesoPezP = pesoPezP + incrementoPesoPezP;
        racionAlimenticiaRequeridaP = pesoPezP * (PescoMultiplicadorEstanque.CalcTasaAlimentacion(pesoPezP) / 100) * PescoMultiplicadorEstanque.EfectTempAlim(temperatura);
        biomasa = pecesEnLoteP * pesoPezP;

        //Sector Oxígeno del agua
        densidadDeGramosPeces = biomasa / AreaEstanque;
        diferenciaOxigeno = nivelDeseadoOxigeno - oxigenoDisuelto;
        gananciaOxigeno= (diferenciaOxigeno> 0) ? tasaAireacion*diferenciaOxigeno : 0.0;
        perdidaOxigenoPorDensidad = oxigenoDisuelto * PescoMultiplicadorEstanque.CalcEfectoDensidad(densidadDeGramosPeces);
        oxigenoDisuelto = oxigenoDisuelto + gananciaOxigeno - perdidaOxigenoPorDensidad;

        //Sector estanque
        muertePeces = (int)Math.Round(pecesEnLoteP * PescoMultiplicadorEstanque.CalcTasaMortalidadFaltaOxigeno(oxigenoDisuelto));
        pecesEnLoteP = pecesEnLoteP - muertePeces;
        pecesMuertos = pecesMuertos + muertePeces;
        
        //Sector Costos
        gastoAlimento = racionAlimenticiaLote * costoAlimento;
        incrementoGastos = gastoAlimento + gastoFijoDiario;
        costoMantenimientoDiario = costoMantenimientoDiario + gastoFijoDiario;
        costoTotalAlimento = costoTotalAlimento + gastoAlimento;
        totalCostos = totalCostos + incrementoGastos;
        datosjuego.SetDineroDisponible(datosjuego.GetDineroDisponible() - gastoFijoDiario);
        //Debug.Log(racionAlimenticia + sp + coverturaPesoMaximo + sp + factorConversion + sp + incrementoPesoPez + sp + pesoPez + sp + gastoComida + sp + incrementoGastos + sp + costos);
        if (muertePeces > 0)
        {
            int inventarioActual = datosjuego.GetInventarioPeces();
            datosjuego.SetInventarioPeces(inventarioActual - muertePeces);
        }
        yield return null;
    }

    public void SumarVenta(double valorVenta, int pecesVendidos)
    {
        totalVentas = totalVentas + valorVenta;
        totalPecesVendidos = totalPecesVendidos + pecesVendidos;
    }

    // --- METODOS GETER AND SETER ----

    // --- Metodos Getter ---

    //Método que permite actualizar los datos del modelo después de efectuada una iteración
    IEnumerator GetDatosModelo()
    {
        //Debug.Log("Datos de Pesco Estanque");
        this.datosModelo[0] = pesoPezP.ToString();
        this.datosModelo[1] = totalCostos.ToString();
        this.datosModelo[2] = consumoRealP.ToString();
        this.datosModelo[3] = coberturaPesoMaximoP.ToString();
        this.datosModelo[4] = factorConversionP.ToString();
        this.datosModelo[5] = incrementoPesoPezP.ToString();
        this.datosModelo[6] = totalAlimentoSuministrado.ToString();
        this.datosModelo[7] = incrementoGastos.ToString();
        this.datosModelo[8] = temperatura.ToString();
        this.datosModelo[9] = racionAlimenticiaRequeridaP.ToString();
        this.datosModelo[10] = racionAlimenticiaASuministrarP.ToString();
        this.datosModelo[11] = racionAlimenticiaSuministradaP.ToString();
        this.datosModelo[12] = oxigenoDisuelto.ToString();
        this.datosModelo[13] = pecesMuertos.ToString();
        this.datosModelo[14] = costoInicialPeces.ToString();
        this.datosModelo[15] = inventarioInicialPeces.ToString();
        this.datosModelo[16] = costoMantenimientoDiario.ToString();
        this.datosModelo[17] = costoTotalAlimento.ToString();
        this.datosModelo[18] = totalVentas.ToString();
        this.datosModelo[19] = totalPecesVendidos.ToString();
        yield return datosModelo;
    }


    public double GetPesoPromedio()
    {
        return this.pesoPezP;
    }

    public double GetFactorConversion()
    {
        return this.factorConversionP;
    }

    public double GetCostoProduccion()
    {
        return this.totalCostos;
    }

    public double GetRacionAlimenticia()
    {
        return this.racionAlimenticiaASuministrarP;
    }

    public double GetRacionAlimenticiaReal()
    {
        return this.consumoRealP;
    }

    public double GetRacionSuministrada()
    {
        return this.racionAlimenticiaSuministradaP;
    }

    public double GetRacionRequerida()
    {
        return this.racionAlimenticiaRequeridaP;
    }

    public int GetPecesEnLoteP()
    {
        return pecesEnLoteP;
    }

    public int GetTiempoIteracion()
    {
        return this.tiempoIteracion;
    }

    public double GetTemperatura()
    {
        return this.temperatura;
    }

    public double GetAreaEstanque()
    {
        return this.AreaEstanque;
    }

    public double GetNivelOxigeno()
    {
        return this.oxigenoDisuelto;
    }

    public double GetDensidadGramosPeces()
    {
        return this.densidadDeGramosPeces;
    }

    public int GetPecesMuertos()
    {
        return this.pecesMuertos;
    }

    public bool GetProduccionActiva()
    {
        return this.produccionActiva;
    }

    public ModeloN5 GetModeloActual()
    {
        return this;
    }

    public double GetCostoInicialPeces()
    {
        return this.costoInicialPeces;
    }

    public double GetCostoTotalMantenimientoDiario()
    {
        return this.costoMantenimientoDiario;
    }

    public double GetCostoTotalAlimento()
    {
        return this.costoTotalAlimento;
    }

    public double GetCostosTotales()
    {
        return this.totalCostos;
    }

    public int GetInventarioInicialPeces()
    {
        return this.inventarioInicialPeces;
    }

    public double GetTotalVentas()
    {
        return this.totalVentas;
    }

    public double GetTotalPecesVendidos()
    {
        return this.totalPecesVendidos;
    }

    public bool GetResumenProduccion()
    {
        return this.resumenProduccion;
    }

    // --- Metodos Setter ---
    //Método que sirve cuando antes de iniciar el juego existe datos guardados del juego.
    public void SetDatosModelo(string[] datosModelo)
    {
        pesoPezP = double.Parse(datosModelo[0]);
        totalCostos = double.Parse(datosModelo[1]);
        consumoRealP = double.Parse(datosModelo[2]);
        coberturaPesoMaximoP = double.Parse(datosModelo[3]);
        factorConversionP = double.Parse(datosModelo[4]);
        incrementoPesoPezP = double.Parse(datosModelo[5]);
        totalAlimentoSuministrado = double.Parse(datosModelo[6]);
        incrementoGastos = double.Parse(datosModelo[7]);

        temperatura = double.Parse(datosModelo[8]);
        racionAlimenticiaRequeridaP = double.Parse(datosModelo[9]);
        racionAlimenticiaASuministrarP = double.Parse(datosModelo[10]);
        racionAlimenticiaSuministradaP = double.Parse(datosModelo[11]);

        oxigenoDisuelto = double.Parse(datosModelo[12]);
        pecesMuertos = int.Parse(datosModelo[13]);

        costoInicialPeces = double.Parse(datosModelo[14]);
        inventarioInicialPeces = int.Parse(datosModelo[15]);
        costoMantenimientoDiario = double.Parse(datosModelo[16]);
        costoTotalAlimento = double.Parse(datosModelo[17]);
        totalVentas = double.Parse(datosModelo[18]);
        totalPecesVendidos = int.Parse(datosModelo[19]);

        biomasa = pesoPezP * pecesEnLoteP;
        densidadDeGramosPeces = biomasa / AreaEstanque;
    }

    public void SetRacionAlimenticiaP(double racion)
    {
        this.racionAlimenticiaASuministrarP = racion;
    }

    public void SetPecesenLoteP(int peces)
    {
        this.pecesEnLoteP = peces;
    }

    public void SetProduccionActiva(bool estado)
    {
        produccionActiva = estado;
    }

    public void SetInventarioAlimento(double nuevoInventario)
    {
        this.inventarioAlimento = nuevoInventario;
    }

    public void SetCostoInicialPeces(double valor)
    {
        this.costoInicialPeces = valor;
    }

    public void SetCostosTotales(double valor)
    {
        this.totalCostos = valor;
    }

    public void SetCostoAlimento(double valor)
    {
        this.costoAlimento = valor;
    }

    public void SetInventarioInicialPeces(int pecesIniciales)
    {
        this.inventarioInicialPeces = pecesIniciales;
    }

    public void SetResumenProduccion(bool estado)
    {
        this.resumenProduccion = estado;
    }

    public void SetTiempoIteracion(int tiempo)
    {
        this.tiempoIteracion = tiempo;
    }

}

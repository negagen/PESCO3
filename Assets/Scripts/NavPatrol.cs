﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavPatrol : MonoBehaviour {

    public Transform[] points=new Transform[4];
    private int destPoint = 0;
    public Transform goal;
    NavMeshAgent agent;

    private void Awake()
    {
        this.points[0] = GameObject.FindGameObjectWithTag("GoalEstanque").GetComponent<Transform>();
        this.points[1] = GameObject.FindGameObjectWithTag("GoalMercado").GetComponent<Transform>();
        this.points[2] = GameObject.FindGameObjectWithTag("GoalInsumos").GetComponent<Transform>();
        this.points[3] = GameObject.FindGameObjectWithTag("GoalGremio").GetComponent<Transform>();
        goal = points[0];
    }
    void Start()
    {

        agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(goal.position);
        // Disabling auto-braking allows for continuous movement
        // between points (ie, the agent doesn't slow down as it
        // approaches a destination point).
        agent.autoBraking = false;

        GotoNextPoint();
    }


    void GotoNextPoint()
    {
        // Returns if no points have been set up
        if (points.Length == 0)
            return;

        // Set the agent to go to the currently selected destination.
        agent.destination = points[destPoint].position;

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        destPoint = (destPoint + 1) % points.Length;
    }


    void Update()
    {
        // Choose the next destination point when the agent gets
        // close to the current one.
        if (agent.remainingDistance < 0.5f)
            GotoNextPoint();
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class PezPlayer : MonoBehaviour {
	public Animator sardineAnimator;
    public Transform trans;
	Rigidbody sardineRigid;
	public float turnSpeed=5f;
	public float forwardSpeed=5f;

    public GameObject opcion1;
    public GameObject opcion2;
    /*public Transform inicialgoal;
    public Transform newgoal;
    NavMeshAgent agent;*/

    public Transform target;
    public float speed=5f;
    private Vector3 force;

    void Start () {
		sardineAnimator = GetComponent<Animator> ();
		sardineAnimator.SetFloat ("Forward", forwardSpeed);
		sardineRigid = GetComponent<Rigidbody> ();
	}
   

	public void TurnLeft(){
        var rotationVector = transform.rotation.eulerAngles;
        rotationVector.y = -90;
        transform.rotation = Quaternion.Euler(rotationVector);

        var offset = target.position - transform.position;
        if (offset.magnitude > .1f)
        {
            offset = offset.normalized * speed;
            sardineRigid.AddForce(offset * Time.deltaTime);
        }
        

        /*target = opcion1.transform;
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);*/
        //sardineRigid.AddTorque (-transform.up*turnSpeed,ForceMode.Impulse);
        sardineAnimator.SetTrigger ("TurnLeft");
    }

	public void TurnRight(){
        //sardineRigid.AddTorque (transform.up*turnSpeed,ForceMode.Impulse);
        var rotationVector = transform.rotation.eulerAngles;
        rotationVector.y = 90;
        transform.rotation = Quaternion.Euler(rotationVector);




        //transform.Translate(Vector3.right * Time.deltaTime);
        //newgoal = opcion2.transform;
        //agent.SetDestination(newgoal.position);
        //Vector3 rot =new Vector3(0.0f,45.0f,0.0f);
        //GetComponent<Transform>().transform.rotation = rot;
        //sardineAnimator.SetTrigger ("TurnRight");
    }

	public void MoveForward(){
		sardineRigid.AddForce (transform.forward*forwardSpeed,ForceMode.Force);
		sardineAnimator.SetTrigger ("MoveForward");
        
	}

	public void TurnUp(){
		sardineRigid.AddTorque (-transform.right*turnSpeed,ForceMode.Impulse);
	}

	public void TurnDown(){
		sardineRigid.AddTorque (transform.right*turnSpeed,ForceMode.Impulse);
	}

	public void setForwardSpeed(float fSpeed){
		forwardSpeed = fSpeed;
		sardineAnimator.SetFloat ("Forward", forwardSpeed);
	}

	public void Move(float v,float h){
		sardineAnimator.SetFloat ("Up", -v);
		sardineAnimator.SetFloat ("Turn", h);

        
        //sardineRigid.AddForce (transform.forward*forwardSpeed);
		//sardineRigid.AddTorque (transform.right*turnSpeed*v);
		//sardineRigid.AddTorque (transform.up*turnSpeed*h);
	}

    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
            sardineRigid.velocity = Vector3.zero;
            sardineRigid.angularVelocity = Vector3.zero;

        if (Input.GetKey("up"))
            sardineRigid.AddForce(0, 0, speed);

        if (Input.GetKey("down"))
            sardineRigid.AddForce(0, 0, -speed);

        if (Input.GetKeyDown(KeyCode.RightArrow))
            sardineRigid.AddForce(speed, 0, 0);

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            sardineRigid.AddForce(-speed, 0, 0);
            Debug.Log("Funcionandoooo");
            force = new Vector3(-speed, 0.0f, 0.0f);
            sardineRigid.AddForceAtPosition(force, opcion1.transform.position, ForceMode.Impulse);
        }
        
    }
}


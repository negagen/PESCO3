﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PezPlayerController : MonoBehaviour {
	PezPlayer pezplayer;
    Transform posicion;
    public int ubicacion;
    Vector3 izquierda;
    Vector3 centro;
    Vector3 derecha;
    public Rigidbody rb;
    public float speed;
    public bool decision;
    //-1 izquierda, 0 centro, 1 derecha

    void Start () {
		pezplayer = GetComponent<PezPlayer> ();
        posicion = this.transform;
        ubicacion = 0;
        rb = GetComponent<Rigidbody>();
        decision = false;
        

    }
	

	void Update () {
        izquierda = new Vector3(-9, this.transform.position.y, this.transform.position.z);
        centro = new Vector3(0, this.transform.position.y, this.transform.position.z);
        derecha = new Vector3(9, this.transform.position.y, this.transform.position.z);
        /* var x = Input.GetAxis("Horizontal") * Time.deltaTime * 500.0f;
         var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

         transform.Rotate(0, x, 0);
         transform.Translate(0, 0, z);*/

        if (Input.GetKeyDown(KeyCode.RightArrow)) {
            if (ubicacion == 0)
            {
                GetComponent<Transform>().position =derecha;
                ubicacion = 1;
            }
            else if (ubicacion == -1)
            {
                GetComponent<Transform>().position = centro;
                ubicacion = 0;
            }
            else if (ubicacion == 1)
            {

            }
            //GetComponent<Transform>().position = new Vector3(10, this.transform.position.y, this.transform.position.z);
            //pezplayer.TurnRight();
        }
		if (Input.GetKeyDown(KeyCode.LeftArrow)) {
            if (ubicacion == 0)
            {
                GetComponent<Transform>().position = izquierda;
                ubicacion = -1;
            }
            else if (ubicacion == -1)
            {
               
            }
            else if (ubicacion == 1)
            {
                GetComponent<Transform>().position = centro;
                ubicacion = 0;
            }
            //pezplayer.TurnLeft();
            //GetComponent<Transform>().position = new Vector3(-10, this.transform.position.y, this.transform.position.z);
        }

        
        var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;
        transform.Translate(0, 0, z);
        /*if (Input.GetKeyDown(KeyCode.DownArrow)) {
            pezplayer.TurnDown();			
		}

		if (Input.GetKeyDown(KeyCode.Space)) {
            pezplayer.MoveForward();		
		}

		if (Input.GetKeyDown(KeyCode.UpArrow)) {
            pezplayer.TurnUp();
		}


		float h = Input.GetAxis ("Horizontal");
		float v = Input.GetAxis ("Vertical");
        pezplayer.Move (v,h);

    */
        
    }

    private void FixedUpdate()
    {
        if (decision == false)
        {
            if (GameObject.FindGameObjectWithTag("Tiempo").GetComponent<PezCountdown>().timeover == true)
            {
                Debug.Log("Impulso");
                //rb.AddForce(13.0f,13.0f,13.0f,ForceMode.Impulse);
                //rb.AddRelativeForce(Vector3.forward*speed);
                rb.AddForce(Vector3.forward*speed, ForceMode.Impulse);
                decision = true;
            }
        }else
        {
            StartCoroutine(reiniciarJuego(5));
        }
        
    }

    IEnumerator reiniciarJuego(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        SceneManager.LoadScene("PracticaAlimentar2");

    }
}

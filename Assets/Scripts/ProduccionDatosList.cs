﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProduccionDatosList : MonoBehaviour {

    public GameObject datoProduccionEntradaPrefab;

    ProduccionManager produccionManager;
    int ultimoContadorCambios;
    
	// Use this for initialization
	void Start () {
        produccionManager = GameObject.FindObjectOfType<ProduccionManager>();
        ultimoContadorCambios = produccionManager.GetContadorCambios();
	}
	
	// Update is called once per frame
	void Update () {
        if(produccionManager==null)
        {
            Debug.LogError("Olvidó agregar el componente ProduccionManager a un gameobject");
            return;
        }

        if(produccionManager.GetContadorCambios()==ultimoContadorCambios)
        {
            //No han ocurrido cambios desde el último Update
            return;
        }

        ultimoContadorCambios = produccionManager.GetContadorCambios();
        while(this.transform.childCount>0)
        {
            Transform c = this.transform.GetChild(0);
            c.SetParent(null);
            Destroy(c.gameObject);
        }

        string[] dias=produccionManager.GetInformacion("peso");

        foreach(string dia in dias)
        {
            GameObject go = (GameObject)Instantiate(datoProduccionEntradaPrefab);
            go.transform.SetParent(this.transform);
            go.transform.Find("Dia").GetComponent<Text>().text = dia;
            go.transform.Find("Peso").GetComponent<Text>().text = produccionManager.GetDato(dia,"peso").ToString();
        }
		
	}
}

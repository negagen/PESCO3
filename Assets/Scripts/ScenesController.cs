﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScenesController : MonoBehaviour {
    public int nivelActual;
    public Text levelname;
    public Text levelcontent;
    public Text levelnews;
    private string nombrenivel;
    private string mensaje;
    private string novedadnivel;

    public GameObject loadingScreen;
    public Slider slider;
    public Text progressText;

    private void Start()
    {
        CargarNivel();
        loadingScreen.SetActive(false);
        CargarInfoNivel();
    }

    public void BotonPlay()
    {
        if (nivelActual == 1)
        {
            LoadLevel("Nivel1");
        }
        else if (nivelActual == 2)
        {
            LoadLevel("Nivel2");
        }
        else if (nivelActual == 3)
        {
            LoadLevel("Nivel3");
        }
        else if (nivelActual == 4)
        {
            LoadLevel("Nivel4");
        }
        else if (nivelActual == 5)
        {
            LoadLevel("Nivel5");
        }
        else if (nivelActual == 6)
        {
            LoadLevel("Nivel6");
        }
        else if (nivelActual == 7)
        {
            LoadLevel("Nivel7");
        } else if (nivelActual == 0)
        {
            LoadLevel("Nivel7");
        }
    }

    public void LoadLevel(string sceneName)
    {
        StartCoroutine(LoadAsynchronously(sceneName));
    }

    IEnumerator LoadAsynchronously (string sceneName)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);
        loadingScreen.SetActive(true);
        while(!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            slider.value = progress;
            progressText.text = "CARGANDO... "+(int)(progress * 100f)+ " %";
            yield return null;
        }
    }

    public void CargarNivel()
    {
        if (GestionBD.singleton.GetEstadoSesion()==true)
        {
            nivelActual = GestionBD.singleton.GetNivel();
        }
        //print(GestionBD.singleton.GetNivel());
    }

    private void CargarInfoNivel()
    {
        if (nivelActual == 1)
        {
            mensaje = "Bienvenido al Nivel 1 de Pesco.";
            nombrenivel = "Producción de un solo pez \n con sistema automatizado";
            novedadnivel = "La primera decisión \n es la venta del pez.";
        }
        else if (nivelActual == 2)
        {
            mensaje = "Bienvenido al Nivel 2 de Pesco.";
            nombrenivel = "Producción de un solo pez \n con ración variable";
            novedadnivel = "Nueva decisión: Suministro de ración \n A partir de este nivel se permite \n  indicar la ración de alimento \n para el pez";
        }
        else if (nivelActual == 3)
        {
            mensaje = "Bienvenido al Nivel 3 de Pesco.";
            nombrenivel = "Producción de lote de peces \n con sistema de producción \n automatizado";
            novedadnivel = "Nueva decisión: Compra de lote de peces \n A partir de este nivel se permite \n  comprar un lote de \n peces";
        }
        else if (nivelActual == 4)
        {
            mensaje = "Bienvenido al Nivel 4 de Pesco.";
            nombrenivel = "Producción de lote de peces \n  con ración variable";
            novedadnivel = "Nueva decisión: Compra de alimento\n A partir de este nivel se explica \n  la relación del alimento con \n el crecimiento del lote de peces";
        }
        else if (nivelActual == 5)
        {
            mensaje = "Bienvenido al Nivel 5 de Pesco.";
            nombrenivel = "Producción de lote de peces \n con ración y temperarura variable";
            novedadnivel = "Nueva decisión: Alimentación en función de la temperatura \n A partir de este nivel se permite \n  la temperatura de la simulación \n varía por cada día de producción";
        }
        else if (nivelActual == 6)
        {
            mensaje = "Bienvenido al Nivel 6 de Pesco.";
            nombrenivel = "Operación en el mercado \n en modalidad asociativa";
            novedadnivel = "Nueva decisión: Venta por turno\n A partir de este nivel se permite \n  la venta por turnos \n en el mercado";
        }
        else if (nivelActual == 7)
        {
            mensaje = "Bienvenido al Nivel 7 de Pesco.";
            nombrenivel = "Operación en el mercado \n en modalidad de demanda fija";
            novedadnivel = "Nueva decisión: Venta por gremio\n A partir de este nivel se permite \n  la venta por demanda fija \n con el gremio ";
        }
        else if(nivelActual==0)
        {
            mensaje = "";
            nombrenivel = "Bienvenido a la modalidad \n de juego libre \n de Pesco";
            novedadnivel = "En este modalidad \n son permitidas todas \n  las operaciones \n tanto de producción \n como de mercadeo";
        }

        levelcontent.text = mensaje;
        levelname.text = nombrenivel;
        levelnews.text = novedadnivel;
    }

}

﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class ScoreManager : MonoBehaviour
{

    // The map we're building is going to look like:
    //
    //	LIST OF USERS -> A User -> LIST OF SCORES for that user
    //

    Dictionary<string, Dictionary<string, int>> playerScores;

    int changeCounter = 0;

    void Start()
    {
    }

    void Init()
    {
        if (playerScores != null)
            return;

        playerScores = new Dictionary<string, Dictionary<string, int>>();
    }

    public void Reset()
    {
        changeCounter++;
        playerScores = null;
    }

    public int GetScore(string username, string scoreType)
    {
        Init();

        if (playerScores.ContainsKey(username) == false)
        {
            // We have no score record at all for this username
            return 0;
        }

        if (playerScores[username].ContainsKey(scoreType) == false)
        {
            return 0;
        }

        return playerScores[username][scoreType];
    }

    public void SetScore(string username, string scoreType, int value)
    {
        Init();

        changeCounter++;

        if (playerScores.ContainsKey(username) == false)
        {
            playerScores[username] = new Dictionary<string, int>();
        }

        playerScores[username][scoreType] = value;
    }

    public void ChangeScore(string username, string scoreType, int amount)
    {
        Init();
        int currScore = GetScore(username, scoreType);
        SetScore(username, scoreType, currScore + amount);
    }

    public string[] GetPlayerNames()
    {
        Init();
        return playerScores.Keys.ToArray();
    }

    public string[] GetPlayerNames(string sortingScoreType)
    {
        Init();

        return playerScores.Keys.OrderByDescending(n => GetScore(n, sortingScoreType)).ToArray();
    }

    public int GetChangeCounter()
    {
        return changeCounter;
    }

    public void DEBUG_ADD_KILL_TO_QUILL()
    {
        ChangeScore("Diana", "kills", 1);
    }

    public void DEBUG_INITIAL_SETUP()
    {
        SetScore("Diana", "kills", 0);
        SetScore("Diana", "assists", 345);

        SetScore("bob", "kills", 10);
        SetScore("bob", "deaths", 14345);

        SetScore("AAAAAA", "kills", 10);
        SetScore("BBBBBB", "kills", 9);
        SetScore("CCCCCC", "kills", 8);
        SetScore("DDDDDD", "kills", 7);
        SetScore("EEEEEE", "kills", 6);
        SetScore("FFFFFF", "kills", 5);
        SetScore("GGGGGG", "kills", 4);
        SetScore("HHHHHH", "kills", 3);
        SetScore("IIIIII", "kills", 3);
        SetScore("JJJJJJ", "kills", 3);
        SetScore("KKKKKK", "kills", 3);
        SetScore("LLLLLL", "kills", 3);
        SetScore("MMMMMM", "kills", 3);
        SetScore("NNNNNN", "kills", 3);
        SetScore("OOOOOO", "kills", 3);
        SetScore("PPPPPP", "kills", 3);
        SetScore("QQQQQQ", "kills", 3);
        SetScore("RRRRRR", "kills", 3);
        SetScore("SSSSSS", "kills", 3);
        SetScore("tttttt", "kills", 3);
        SetScore("uuuuuu", "kills", 3);
        SetScore("vvvvvv", "kills", 3);
        SetScore("wwwwww", "kills", 3);
        SetScore("xxxxxx", "kills", 3);
        SetScore("yyyyyy", "kills", 3);
        SetScore("zzzzzz", "kills", 3);/**/


        Debug.Log(GetScore("Diana", "kills"));
    }

}

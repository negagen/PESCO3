﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;

public class SesionesList : MonoBehaviour {

    public GameObject sesionEntryPrefab;
    public Dictionary<string, int> sesionesActivas;
    public string[] listadoSesiones;

    // Use this for initialization
    void Start () {
        listadoSesiones = GestionBD.singleton.GetSesiones();

        Init();
        CrearSesiones();
	}

    void Init()
    {
        if (sesionesActivas != null)
            return;

        sesionesActivas = new Dictionary<string, int>();
    }

    private void CrearSesiones()
    {
        for(int i=0; i < listadoSesiones.Length; i++)
        {
            string[] nDatosSesion = listadoSesiones[i].Split('&');
            
            GameObject go = (GameObject)Instantiate(sesionEntryPrefab);
            go.transform.SetParent(this.transform);
            go.transform.Find("NombreSesion").GetComponent<Text>().text = nDatosSesion[0];
            go.transform.Find("NivelSesion").GetComponent<Text>().text = nDatosSesion[1];
            Button btn = go.transform.Find("btnJugar(Button)").GetComponent<Button>();
            btn.onClick.AddListener(delegate { TaskOnClick(nDatosSesion); });
            go.SetActive(true);
        }
    }

    void TaskOnClick(string[] partidaSeleccionada)
    {
        GestionBD.singleton.SetSesionActual(partidaSeleccionada[0]);
        GestionBD.singleton.SetNivelActual(int.Parse(partidaSeleccionada[1]));
        GestionBD.singleton.SetTiempoIteracion(int.Parse(partidaSeleccionada[2]));
        GestionBD.singleton.SetTiempoMercado(int.Parse(partidaSeleccionada[3]));
        GestionBD.singleton.ConsultarDatosBase();
        SceneManager.LoadScene("Panel");
    }

    // Update is called once per frame
    void Update () {
        
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

public class Temporizador : MonoBehaviour {

    public PlayTimer tiempo;
    public Image LoadingBar;
    public Transform txtTiempoIteracion;
    [SerializeField]
    private float timeAmount=2;
    [SerializeField]
    private float time;
    [SerializeField]
    private int duracionDia;

    private void Start()
    {
        LoadingBar = this.GetComponent<Image>();
        duracionDia = tiempo.GetDuracionDia();
        //timeAmount = duracionDia;
        timeAmount = tiempo.tiempoRestante;
        time = timeAmount;
        //time = tiempo.tiempoRestante;
    }

    void Update () {
        if (duracionDia != tiempo.GetDuracionDia())
        {
            duracionDia = tiempo.GetDuracionDia();
            time = duracionDia - tiempo.GetPlaytime();
        }
        if (tiempo.GetTiempoDetenido())
        {
            duracionDia = 0;
        }
        timeAmount = duracionDia;
        //timeAmount = tiempo.tiempoRestante;
        if (time > 0)
        {
            //txtTiempoIteracion.GetComponent<Text>().text = ((int)time+1).ToString() + " s";
            //time = tiempo.tiempoRestante;
            time -= Time.deltaTime;
            LoadingBar.fillAmount = 1 - (time / timeAmount);

        }
        else if (time <= 0)
        {
            //timeAmount = duracionDia;
            time = timeAmount;
            //time = tiempo.tiempoRestante;
        }
    }
        
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class UIControllerN1 : MonoBehaviour {

    //Modelo vigente
    public GameLogicN1 datosjuego;
    public ModeloN1 modelo;
    public Text nombreJugador;
    public Text nombreSesion;
    public Text HUDNivel;
    public Image HUDLogoPesco;
    //Campos que cambian del HUD
    public Text HUDPeces;
    public Text HUDDinero;
    public Text HUDDia;
    //Campos que cambian de la UI
    //Campos que cambian de los paneles de cada escenario del mapa
    //Panel del Estanque
    public Text PEPeso;
    public Text PEConsumoActual;
    public Text PEFactorConversion;
    public Text PECosto;
    public Text PECostoKg;
    //PE - Pestaña Ración
    public Text PERacionRequerida;
    public Text PERacionASuministrar;
    public Text PERacionSuministrada;
    public Text PETotalAlimentoSuministrado;
    //Panel del Mercado
    public Text PMCostosTotales;
    public Text PMCostoKg;
    public Text PMBiomasa;
    public Text PMPrecioKg;
    private string vacio = "...";

    //Módulo de duración del día
    public Text SDDValorDia;

    //Mensajes de error y alerta
    private bool controlAnimacionProduccion;
    public Image imgErrorPesoMinimo;
    public Image imgErrorConexion;
    public Image imgGuardarEstado;

    // Use this for initialization
    void Start () {
        modelo.GetModeloActual();
        controlAnimacionProduccion = false;
    }
	
	// Update is called once per frame
	void Update () {
        ReproducirAnimacionInicioProduccion(datosjuego.GetEstadoProduccion());
        //Error Peso
        if (datosjuego.GetErrorPeso() == true)
        {
            imgErrorPesoMinimo.gameObject.SetActive(true);
        }
        //Error Conexión
        if (datosjuego.GetErrorConexion() == true)
        {
            imgErrorConexion.gameObject.SetActive(true);
        }
        //Guardando partida
        if (datosjuego.GetGuardandoPartida())
        {
            imgGuardarEstado.gameObject.SetActive(true);
        }
        else
        {
            imgGuardarEstado.gameObject.SetActive(false);
        }
    }

    private void OnGUI()
    {
        nombreJugador.text = GestionBD.singleton.GetNombreJugador();
        nombreSesion.text = GestionBD.singleton.GetNombreSesion();
        HUDNivel.GetComponent<Text>().text = "NIVEL 1";
        HUDDinero.GetComponent<Text>().text = datosjuego.GetDineroDisponible().ToString("N2");
        PMPrecioKg.GetComponent<Text>().text = "$ " + datosjuego.GetPrecioVentaPez().ToString("N0");
        //SDDValorDia.text = modelo.timer.getDuracionDia().ToString();
        //Actualización de textos en el juego
        if (modelo.GetProduccionActiva() == true)
        {
            //Actualización de datos del HUD
            HUDPeces.text = modelo.GetPecesEnLoteP().ToString();
            HUDDia.text = modelo.GetTiempoIteracion().ToString();
            //Actualización de datos de Paneles
            PEPeso.GetComponent<Text>().text = AjustarGramos(modelo.GetPesoPromedio());
            PEConsumoActual.GetComponent<Text>().text = AjustarGramos(modelo.GetConsumoActual());
            PEFactorConversion.GetComponent<Text>().text = Math.Round(modelo.GetFactorConversion(), 3).ToString();
            PECosto.GetComponent<Text>().text = "$ " + Math.Round(modelo.GetCostoProduccion(), 2).ToString("N0");
            PECostoKg.GetComponent<Text>().text = "$ " + Math.Round((modelo.GetCostoProduccion()) / (modelo.GetPesoPromedio()/1000f), 2).ToString("N0");

            PERacionRequerida.GetComponent<Text>().text = AjustarGramos(modelo.GetRacionRequerida());
            PERacionASuministrar.GetComponent<Text>().text = AjustarGramos(modelo.GetRacionAlimenticia());
            PERacionSuministrada.GetComponent<Text>().text = AjustarGramos(modelo.GetRacionSuministrada());
            PETotalAlimentoSuministrado.GetComponent<Text>().text = AjustarGramos(modelo.GetTotalAlimentoSuministrado());

            PMCostosTotales.GetComponent<Text>().text = "$ " + Math.Round(modelo.GetCostoProduccion(),2).ToString();
            PMCostoKg.GetComponent<Text>().text = "$ " + Math.Round((modelo.GetCostoProduccion()) / (modelo.GetPesoPromedio()/1000f), 2).ToString("N0");
            PMBiomasa.GetComponent<Text>().text = Math.Round(modelo.GetPesoPromedio(),2).ToString() + " g";
            SDDValorDia.GetComponent<Text>().text = modelo.timer.GetDuracionDia().ToString()+" s";
        }
        else
        {
            //Actualización de datos del HUD
            HUDPeces.text = "-";
            HUDDia.text = "-";
            HUDDia.gameObject.SetActive(false);
            HUDLogoPesco.gameObject.SetActive(true);
            //Actualización de datos de Paneles
            PEPeso.GetComponent<Text>().text = vacio;
            PEConsumoActual.GetComponent<Text>().text = vacio;
            PEFactorConversion.GetComponent<Text>().text = vacio;
            PECosto.GetComponent<Text>().text = vacio;
            PMCostosTotales.GetComponent<Text>().text = vacio;
            PECostoKg.GetComponent<Text>().text = vacio;
            PETotalAlimentoSuministrado.GetComponent<Text>().text = vacio;
            PERacionRequerida.text = vacio;
            PERacionASuministrar.text = vacio;
            PERacionSuministrada.text = vacio;
            PMCostoKg.GetComponent<Text>().text = vacio;
        }
    }

    private void ReproducirAnimacionInicioProduccion(bool estado)
    {
        if (estado)
        {
            if (controlAnimacionProduccion)
            {
                StartCoroutine(IniciarAnimacionProduccion());
                controlAnimacionProduccion = false;
            }
        }
        else
        {
            controlAnimacionProduccion = true;
        }
    }

    IEnumerator IniciarAnimacionProduccion()
    {
        HUDDia.gameObject.SetActive(false);
        HUDLogoPesco.gameObject.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(3f);
        controlAnimacionProduccion = false;
        HUDLogoPesco.gameObject.SetActive(false);
        HUDDia.gameObject.SetActive(true);
    }

    private string AjustarGramos(double valor)
    {
        string mensaje;
        if (valor < 1000)
        {
            mensaje = Math.Round(valor, 2).ToString() + " g";
        }
        else
        {
            mensaje = Math.Round(valor / 1000f, 3).ToString() + " kg";
        }
        return mensaje;
    }

    public void CerrarVentanaErrorPesoMinimo()
    {
        imgErrorPesoMinimo.gameObject.SetActive(false);
        datosjuego.SetErrorPeso(false);
    }

    public void CerrarVentanaErrorConexion()
    {
        imgErrorConexion.gameObject.SetActive(false);
        datosjuego.SetErrorConexion(false);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class ViewCamEstanque : MonoBehaviour
{
    public AudioMixerSnapshot bajoagua;
    public AudioMixerSnapshot corriendo;
    public Canvas canvasEstanque;
    public GameObject vistaEstanque;
    // Use this for initialization
    void Start()
    {
        //vistaMundo = GameObject.FindGameObjectWithTag("Main Camera");
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            canvasEstanque.gameObject.SetActive(true);
            vistaEstanque.gameObject.SetActive(true);
            GetComponent<AudioSource>().Play();
            bajoagua.TransitionTo(0.05f);
        }
    }

    void OnTriggerExit(Collider other)
    {
        canvasEstanque.gameObject.SetActive(false);
        vistaEstanque.gameObject.SetActive(false);
        corriendo.TransitionTo(0.05f);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewCamInsumos : MonoBehaviour
{

    public GameObject vistaInsumos;
    // Use this for initialization
    void Start()
    {
        //vistaMundo = GameObject.FindGameObjectWithTag("Main Camera");
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            vistaInsumos.gameObject.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        vistaInsumos.gameObject.SetActive(false);
    }
    
}

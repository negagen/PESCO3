﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewCamMercado : MonoBehaviour
{

    public GameObject vistaMercado;
    // Use this for initialization
    void Start()
    {
        //vistaMundo = GameObject.FindGameObjectWithTag("Main Camera");
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            vistaMercado.gameObject.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        vistaMercado.gameObject.SetActive(false);
    }
    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ViewIntro : MonoBehaviour {
    [Tooltip("Jugador principal.")]
    public GameObject player;
    [Tooltip("Head up display. Mostrar el HUD.")]
    public Canvas hud;
    [Tooltip("Botón de skip. Ocultar el Canvas de Intro.")]
    public GameObject skip;
    private Animation animacionIntro;

    private void Start()
    {
        hud.gameObject.SetActive(false);
        animacionIntro = gameObject.GetComponent<Animation>();
    }

    private void Update()
    {
        if (!animacionIntro.isPlaying)
        {
            SaltarIntro();
            IniciarPartida();
        }
    }
    
    public void SaltarIntro()
    {
        Object.Destroy(GameObject.FindGameObjectWithTag("VistaIntro"));
        GameObject.Destroy(skip);
    }

    public void IniciarPartida()
    {
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CamaraControllerCapsule>().enabled = true;
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().enabled = true;
        hud.gameObject.SetActive(true);
        player.SetActive(true);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewUI : MonoBehaviour {

    //Script encargado de la parte visual del GUI del juego

    public Button btnVerPausa;
    public Image imgOpcionesPausa;
    public Button btnVerConfiguracion;
    public Image imgOpcionesConfiguracion;
    public Button btnVerCamaras;
    public Image imgBarraCamaras;
    public Button btnMisiones;
    public Image imgMisiones;

    private bool actPausa;
    private bool actConfiguracion;
    private bool actCamaras;
    private bool actMisiones;

    public GameObject pnlVistaCentro;
    public Image imgVerAyuda;
    public Image imgInstrucciones;
    public Image imgControles;
    //Panel de Estanque
    public Image listarInfo;
    public Image listarChange;
    public Image listarInfoEstanque;
    public Image listarCostosEstanque;
    public Image listarTime;

    //Panel de Insumos
    public Image comprarPeces;
    public Image comprarAlimento;

    [SerializeField]
    private Canvas[] listadoCanvas;
    [SerializeField]
    private Canvas canvasActual;


    // Use this for initialization
    void Start () {
        actCamaras = false;
        actConfiguracion = false;
        actPausa = false;
        IndicarCanvasActivo();
    }

    //Función para conocer qué canvas estba activo al momento de pausar o configurar
    private void IndicarCanvasActivo()
    {
        listadoCanvas = GameObject.FindObjectsOfType<Canvas>();
        canvasActual = listadoCanvas[0];
        foreach (Canvas elemento in listadoCanvas)
        {
            if (!elemento.gameObject.CompareTag("HUD"))
            {
                canvasActual = elemento;
            }
        }
        //print(canvasActual);
    }

    private void Update()
    {
        if (actPausa && !imgVerAyuda.gameObject.activeInHierarchy)
        {
            imgOpcionesPausa.gameObject.SetActive(true);
        }
    }
       
    public void VerPausa()
    {
        if (actPausa == false)
        {
            IndicarCanvasActivo();
            //Activo los botones
            if (!canvasActual.gameObject.CompareTag("HUD"))
            {
                canvasActual.gameObject.SetActive(false);
            }
            imgOpcionesPausa.gameObject.SetActive(true);
            actPausa = true;
        }else
        {
            //Desactivo los botones
            if (!canvasActual.gameObject.CompareTag("HUD"))
            {
                canvasActual.gameObject.SetActive(true);
            }
            imgOpcionesPausa.gameObject.SetActive(false);
            actPausa = false;
        }
    }

    public void VerConfiguracion()
    {
        if (actConfiguracion == false)
        {
            //Activo los botones
            IndicarCanvasActivo();
            if (!canvasActual.gameObject.CompareTag("HUD"))
            {
                canvasActual.gameObject.SetActive(false);
            }
            imgOpcionesConfiguracion.gameObject.SetActive(true);
            actConfiguracion = true;
        }
        else
        {
            canvasActual.gameObject.SetActive(true);
            //Desactivo los botones
            imgOpcionesConfiguracion.gameObject.SetActive(false);
            actConfiguracion = false;
        }
    }

    public void VerCamaras()
    {
        if (actCamaras == false)
        {
            //Activo los botones
            btnVerCamaras.gameObject.SetActive(false);
            //vistaCentro.SetActive(false);
            imgBarraCamaras.gameObject.SetActive(true);
            actCamaras = true;
        }
        else
        {
            //Desactivo los botones
            btnVerCamaras.gameObject.SetActive(true);
            //vistaCentro.SetActive(true);
            imgBarraCamaras.gameObject.SetActive(false);
            actCamaras = false;
        }
    }

    public void VerMisiones()
    {
        if (actMisiones == false)
        {
            //Activo los botones
            IndicarCanvasActivo();
            if (!canvasActual.gameObject.CompareTag("HUD"))
            {
                canvasActual.gameObject.SetActive(false);
            }
            imgMisiones.gameObject.SetActive(true);
            actMisiones = true;
        }
        else
        {
            canvasActual.gameObject.SetActive(true);
            //Desactivo los botones
            imgMisiones.gameObject.SetActive(false);
            actMisiones = false;
        }
    }

    //Panel del estanque
    public void VerInfo()
    {
        listarInfo.gameObject.SetActive(true);
        listarChange.gameObject.SetActive(false);
        listarInfoEstanque.gameObject.SetActive(false);
        listarCostosEstanque.gameObject.SetActive(false);
        listarTime.gameObject.SetActive(false);
    }

    public void VerCambioDatos()
    {
        //Mostrar ventana para variar datos del modelo (ración)
        listarInfo.gameObject.SetActive(false);
        listarChange.gameObject.SetActive(true);
        listarInfoEstanque.gameObject.SetActive(false);
        listarCostosEstanque.gameObject.SetActive(false);
        listarTime.gameObject.SetActive(false);
    }

    public void VerInfoEstanque()
    {
        //Mostrar ventana para ver información del estanque
        listarInfo.gameObject.SetActive(false);
        listarChange.gameObject.SetActive(false);
        listarInfoEstanque.gameObject.SetActive(true);
        listarCostosEstanque.gameObject.SetActive(false);
        listarTime.gameObject.SetActive(false);
    }

    public void VerCostosProduccion()
    {
        //Mostrar ventana para costos de producción
        listarInfo.gameObject.SetActive(false);
        listarChange.gameObject.SetActive(false);
        listarInfoEstanque.gameObject.SetActive(false);
        listarCostosEstanque.gameObject.SetActive(true);
        listarTime.gameObject.SetActive(false);
    }
    public void VerCambioTiempo()
    {
        //Mostrar ventana tiempo
        listarInfo.gameObject.SetActive(false);
        listarChange.gameObject.SetActive(false);
        listarInfoEstanque.gameObject.SetActive(false);
        listarCostosEstanque.gameObject.SetActive(false);
        listarTime.gameObject.SetActive(true);
    }

    public void VerComprarPeces()
    {
        //Mostrar ventana de compra de peces
        comprarPeces.gameObject.SetActive(true);
        comprarAlimento.gameObject.SetActive(false);
    }

    public void VerComprarAlimento()
    {
        //Mostrar ventana de compra de alimento
        comprarPeces.gameObject.SetActive(false);
        comprarAlimento.gameObject.SetActive(true);
    }

    public void CerrarCambioTiempo()
    {
        listarTime.gameObject.SetActive(false);
    }

    public void CerrarAyuda()
    {
        imgVerAyuda.gameObject.SetActive(false);
    }

    public void CerrarMisiones()
    {
        imgMisiones.gameObject.SetActive(false);
    }

    public void VerAyuda()
    {
        if (actPausa == true)
        {
            imgVerAyuda.gameObject.SetActive(true);
        }
        else
        {
            imgVerAyuda.gameObject.SetActive(false);
        }
    }

    public void VerControles()
    {
        imgControles.gameObject.SetActive(true);
        imgInstrucciones.gameObject.SetActive(false);
    }

    public void VerInstrucciones()
    {
        imgControles.gameObject.SetActive(false);
        imgInstrucciones.gameObject.SetActive(true);
    }

}

